# Random Multi-Type Spanning Forests for Synchronization on Sparse Graphs

This repository contains the code used in our eponymous paper.

> Hugo Jaquard, Pierre-Olivier Amblard, Simon Barthelmé, Nicolas Tremblay   
> Random Multi-Type Spanning Forests for Synchronization on Sparse Graphs   
> https://arxiv.org/abs/2403.19300   

A partial documentation can be found [here](https://gaia.gricad-pages.univ-grenoble-alpes.fr/synchromtsf/).

Corresponding author's e-mail: hugo DOT jaquard AT gipsa-lab.grenoble-inp DOT fr.
