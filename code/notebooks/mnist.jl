### A Pluto.jl notebook ###
# v0.19.29

using Markdown
using InteractiveUtils

# ╔═╡ 26e0539a-fcce-4109-86ce-1e074f2eb768
import Pkg; Pkg.add("BenchmarkTools")

# ╔═╡ d2d6686d-126b-43fe-9859-cce47cd37558
Pkg.add("ImageFeatures");Pkg.add("TestImages")

# ╔═╡ f3a05b68-b5fc-11ee-116c-1341156066b9
using BenchmarkTools,Graphs,LinearAlgebra,StatsBase,SparseArrays,SpecialFunctions,Plots,Random, Distributions,KrylovKit,JLD,PGFPlotsX

# ╔═╡ 80a0fa5e-ade5-4b4c-8d45-c6a6f1ff5660
using ImageFeatures, TestImages, Images, ImageDraw, ImageView, CoordinateTransformations, Rotations

# ╔═╡ 257292ff-98b7-4465-85d6-db270b6f3e95
using MLDatasets

# ╔═╡ 00d90060-bf13-4ae3-a26a-b68643349112
include("../src/header.jl")

# ╔═╡ a51109d2-4f39-4ba8-9017-b4b42c99bf92
# outputs the (p,q)-th image moment of img
function moment(img::Matrix{Gray{Float64}},p::Int64,q::Int64)
	m = Float64(0)

	for x in 1:size(img)[1]
		for y in 1:size(img)[2]
			m += x^p * y^q * img[x,y]
		end
	end

	return m
end

# ╔═╡ 7366143a-098e-4fa7-82ae-8769f5541991
# outputs the (p,q)-th central image moment of img
function central_moment(img::Matrix{Gray{Float64}},p::Int64,q::Int64)
	m00 = moment(img,0,0)
	x_mean = moment(img,1,0)/m00
	y_mean = moment(img,0,1)/m00

	m = Float64(0)
	
	for x in 1:size(img)[1]
		for y in 1:size(img)[2]
			m += (x-x_mean)^p * (y-y_mean)^q * img[x,y]
		end
	end

	return m
end

# ╔═╡ 4326b731-ad6f-4a60-8adc-35adb9b27c90
# outputs the covariance matrice of img
function cov_matrix(img::Matrix{Gray{Float64}})
	cov = zeros(Float64,(2,2))
	
	m20 = central_moment(img,2,0)
	m02 = central_moment(img,0,2)
	m11 = central_moment(img,1,1)
	m00 = central_moment(img,0,0)
	
	cov[1,1] = m20/m00
	cov[2,2] = m02/m00
	cov[1,2] = m11/m00
	cov[2,1] = m11/m00
	return Hermitian(cov)
end

# ╔═╡ 773ad8bf-7b66-46f3-9a17-59279dc6a09b
# apply a rotation of specified angle to img
function rotate(img::Matrix{Gray{Float64}},angle::Float64)
	return imrotate(img,angle,axes(img),fillvalue=0)
end

# ╔═╡ a855d8d0-b3f9-4542-b55c-3aabb3be5ce8
# outputs the connection angle from img1 to img2
function registration_angle(img1::Matrix{Gray{Float64}},img2::Matrix{Gray{Float64}})
	cov1 = cov_matrix(img1)
	cov2 = cov_matrix(img2)
		
	cov_eig1 = eigen(cov1)
	cov_eig2 = eigen(cov2)

	angle1 = atan(cov_eig1.vectors[2,1],cov_eig1.vectors[1,1])
	angle2 = atan(cov_eig2.vectors[2,1],cov_eig2.vectors[1,1])

	return (angle1 - angle2) % (2*pi)
end

# ╔═╡ 6d5fcd6c-3247-496f-b75d-b834482795ca
function denoise(imgs::Vector{Matrix{Gray{Float64}}},angles::Vector{Float64})
	output = rotate(imgs[1],angles[1])

	for i in 2:length(imgs)
		output += rotate(imgs[i],angles[i])
	end

	return output/length(imgs)
end

# ╔═╡ e91a7b88-60fa-4eb2-8955-e7b83f9d199d
function sync_denoise_mtsf(SyncG::ConnectionGraph,init::Vector{ComplexF64},imgs::Vector{Matrix{Gray{Float64}}},sync_m::Int64,sync_k::Int64,sync_q::Float64)
	sync_q_vector = sync_q * ones(SyncG.n)
	
	phi = SmoothingMTSF(SyncG.n)
	buf = SamplingBuffer(SyncG.n)
	
	tmp_f = Vector{ComplexF64}(undef,SyncG.n)
	
	sync_eig = power_method(init,(x,y) -> _rb_smooth_with_MTSF!(SyncG,sync_q_vector,init,sync_m,phi,buf,y,tmp_f),nb_steps = sync_k)

	reconstruction_angles = angle.(sync_eig)
	
	denoised_image = denoise(imgs,reconstruction_angles)

	return denoised_image, sync_eig
end

# ╔═╡ 10043ddb-b80a-410e-81f1-d1c28b07fe43
function sync_denoise_exact(SyncG::ConnectionGraph,init::Vector{ComplexF64},imgs::Vector{Matrix{Gray{Float64}}})
	sync_eig = eigsolve(SyncG.L,1,:SR,maxiter=10000)[2][1]
	
	reconstruction_angles = angle.(sync_eig)
	
	denoised_image = denoise(imgs,reconstruction_angles)

	return denoised_image, sync_eig
end

# ╔═╡ 62ce2626-26b6-4d59-865b-0b36387f8bc0
# now the experiment
# TODO: identifier le bon digit et le nb4 pour ce digit

# ╔═╡ 1bb1587f-f54c-4682-b4d0-6845caeded9e
begin
	mnist = MNIST(Tx=Float64)
	nb4 = 5000
	
	digit4 = Vector{Matrix{Gray{Float64}}}(undef,nb4)

	idx = 1
	for i in 1:length(mnist)
		if mnist.targets[i] == 1 && idx <= nb4
			digit4[idx] = Gray.(transpose(mnist.features[:,:,i]))
			idx += 1
		end
	end
end

# ╔═╡ 6df4f0e0-a77f-405b-a16f-4768d72e0d58
length(mnist.targets)

# ╔═╡ 0b5f2354-cf4d-4b88-952e-be7efee1c323
digit4[1]

# ╔═╡ f77a4c9c-b73b-440a-b30f-d2c3172d388b
digit4[2]

# ╔═╡ 1263d3bb-2258-44c0-b1b6-29cd7c505cf3
# construction du nearest-neighbor graph et de la connection
begin
	s = 35 # nb of nearest neighbors, degree of the graph

	# init graph
	g = Graph{UInt64}(nb4)

	#init s-nearest-neighbors vect (size n * s)
	NL = Vector{Vector{UInt64}}(undef,nb4)
	for i in 1:nb4
		NL[i] = zeros(UInt64,s)
	end

	#init distance list
	cst = 28*28*255 # bound on the distance between images
	DL = Vector{Vector{Float64}}(undef,nb4)
	for i in 1:nb4
		DL[i] = cst * ones(Float64,s)
	end
	
	#init also rotation list
	RL = Vector{Vector{Float64}}(undef,nb4)
	for i in 1:nb4
		RL[i] = zeros(Float64,s)
	end

	rot = 0.
	d = 0.
	
	#fill NN vect and rot list
	for i in 1:nb4
		for j in 1:nb4 #nb4 # VÉRIFIER QUE CORRECT
			if i != j
				rot = registration_angle(digit4[i],digit4[j]) % pi # pourquoi % pi ???
				d = norm(digit4[i] - rotate(digit4[j],-rot))

				if d < maximum(DL[i])
					am = argmax(DL[i])
					NL[i][am] = j
					DL[i][am] = d
					RL[i][am] = -rot # ou -rot, selon convention à établir
				end
			end
		end
	end
end

# ╔═╡ cec61539-73ec-4fbd-955c-ba056ee957c5
NL

# ╔═╡ 49634890-bf77-4a77-9e47-5ec15beb0275
RL

# ╔═╡ e6b96bd2-978f-4617-bfdd-ddc0c2dec9c3
DL

# ╔═╡ 9ef7a0da-9bd2-44b6-9fe9-bb7319d47094
begin
	obs_matrix = spzeros(ComplexF64,(nb4,nb4))

	#build NN graph (symmetric version)
	#note: could used a weighted graph (like exp(-norm))
	
	for i in 1:nb4
		for j in 1:s
			add_edge!(g,i,NL[i][j])
			add_edge!(g,NL[i][j],i)
	
			obs_matrix[i,NL[i][j]] = exp(im*RL[i][j]) # ou l'opposé, selon convention
			obs_matrix[NL[i][j],i] = conj(obs_matrix[i,NL[i][j]]) 
		end
	end

	SyncG = ConnectionGraph(g,sparse(unit_part.(obs_matrix)),sparse(abs.(obs_matrix)))

	init = unit_part.(randn(ComplexF64,SyncG.n))
end

# ╔═╡ 54f1cee9-952f-4d89-80c3-d34909786f70
typeof(SyncG)

# ╔═╡ 4773031a-de1c-49e9-8a81-86fb182ed22f
abs.(obs_matrix)

# ╔═╡ b10c6b30-70cd-43af-9a11-4e481b3fa552
is_connected(g)

# ╔═╡ 7732a91d-9960-44b3-b2e5-d16106e434b2
begin
	denoised_image,exact_eig = sync_denoise_exact(SyncG,init,digit4)
	denoised_image
end

# ╔═╡ f2ce5801-0dc6-4eef-a7aa-2234ac69b932
begin
	sync_q = 0.0001 * mean(degree(SyncG.g))
	q_vector = sync_q * ones(nv(SyncG.g))
	sync_k = 10
	sync_m = 5
	
	mtsf_denoised, mtsf_eig = sync_denoise_mtsf(SyncG,init,digit4,sync_m,sync_k,sync_q)

	mtsf_denoised
end

# ╔═╡ bce6d390-3d03-4cfd-a499-818669e11f93
not_aligned_denoised = denoise(digit4,zeros(nb4))

# ╔═╡ d1310e54-c468-4a54-8761-c3d5066b9519
begin
	#save("../plots/mnist/mtsf_denoised.png",map(clamp01nan,mtsf_denoised))
	#save("../plots/mnist/img_denoised.png",map(clamp01nan,denoised_image))
	#save("../plots/mnist/norot_denoised.png",map(clamp01nan,not_aligned_denoised))
end

# ╔═╡ 471fd968-34fd-46fc-9dd6-ef8b5cf5cd7d
digit4[15]

# ╔═╡ af55201c-a209-468b-a4d4-8806dbf1db76
digit4[1]

# ╔═╡ 53ba22ca-d749-4006-8249-c489aa11cb66
digit4[10]

# ╔═╡ 9c06cd58-cbb2-4dc3-9d91-53b7be65eda9
begin
	r_mtsf = registration_angle(digit4[15],mtsf_denoised)
	final_mtsf = rotate(mtsf_denoised,-r_mtsf)

	r_exact = registration_angle(digit4[15],denoised_image)
	final_exact = rotate(denoised_image,-r_exact)

	save("../plots/mnist/mtsf_denoised.png",map(clamp01nan,final_mtsf))
	save("../plots/mnist/img_denoised.png",map(clamp01nan,final_exact))
end

# ╔═╡ 04504d98-d73f-4114-8bcc-61f51a93fb7b
begin
	save("../plots/mnist/sample1.png",map(clamp01nan,digit4[15]))
	save("../plots/mnist/sample2.png",map(clamp01nan,digit4[10]))
	save("../plots/mnist/sample3.png",map(clamp01nan,digit4[1]))
end

# ╔═╡ eed80bc0-5755-4981-9574-fb7b1d9b911d
function pm_adj(A::SparseMatrixCSC{ComplexF64,Int64},init::Vector{ComplexF64},k::Int64)
	input = copy(init)
	output = Vector{ComplexF64}(undef,length(input))

	i = Int64(1)
	while i <= k
		output = A*input
		output ./= norm(output)

		i += 1
		input = output
	end
	return output
end

# ╔═╡ Cell order:
# ╠═26e0539a-fcce-4109-86ce-1e074f2eb768
# ╠═f3a05b68-b5fc-11ee-116c-1341156066b9
# ╠═d2d6686d-126b-43fe-9859-cce47cd37558
# ╠═80a0fa5e-ade5-4b4c-8d45-c6a6f1ff5660
# ╠═257292ff-98b7-4465-85d6-db270b6f3e95
# ╠═00d90060-bf13-4ae3-a26a-b68643349112
# ╠═a51109d2-4f39-4ba8-9017-b4b42c99bf92
# ╠═7366143a-098e-4fa7-82ae-8769f5541991
# ╠═4326b731-ad6f-4a60-8adc-35adb9b27c90
# ╠═773ad8bf-7b66-46f3-9a17-59279dc6a09b
# ╠═a855d8d0-b3f9-4542-b55c-3aabb3be5ce8
# ╠═6d5fcd6c-3247-496f-b75d-b834482795ca
# ╠═e91a7b88-60fa-4eb2-8955-e7b83f9d199d
# ╠═10043ddb-b80a-410e-81f1-d1c28b07fe43
# ╠═62ce2626-26b6-4d59-865b-0b36387f8bc0
# ╠═1bb1587f-f54c-4682-b4d0-6845caeded9e
# ╠═6df4f0e0-a77f-405b-a16f-4768d72e0d58
# ╠═0b5f2354-cf4d-4b88-952e-be7efee1c323
# ╠═f77a4c9c-b73b-440a-b30f-d2c3172d388b
# ╠═1263d3bb-2258-44c0-b1b6-29cd7c505cf3
# ╠═cec61539-73ec-4fbd-955c-ba056ee957c5
# ╠═49634890-bf77-4a77-9e47-5ec15beb0275
# ╠═e6b96bd2-978f-4617-bfdd-ddc0c2dec9c3
# ╠═9ef7a0da-9bd2-44b6-9fe9-bb7319d47094
# ╠═54f1cee9-952f-4d89-80c3-d34909786f70
# ╠═4773031a-de1c-49e9-8a81-86fb182ed22f
# ╠═b10c6b30-70cd-43af-9a11-4e481b3fa552
# ╠═7732a91d-9960-44b3-b2e5-d16106e434b2
# ╠═f2ce5801-0dc6-4eef-a7aa-2234ac69b932
# ╠═bce6d390-3d03-4cfd-a499-818669e11f93
# ╠═d1310e54-c468-4a54-8761-c3d5066b9519
# ╠═471fd968-34fd-46fc-9dd6-ef8b5cf5cd7d
# ╠═af55201c-a209-468b-a4d4-8806dbf1db76
# ╠═53ba22ca-d749-4006-8249-c489aa11cb66
# ╠═9c06cd58-cbb2-4dc3-9d91-53b7be65eda9
# ╠═04504d98-d73f-4114-8bcc-61f51a93fb7b
# ╠═eed80bc0-5755-4981-9574-fb7b1d9b911d
