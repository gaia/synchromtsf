### A Pluto.jl notebook ###
# v0.19.26

using Markdown
using InteractiveUtils

# ╔═╡ 6a19910f-0090-42ad-a51f-65e5cd20916e
using Pkg; Pkg.add(url="https://github.com/lorenzodallamico/CoDeBetHe.jl.git"); using CoDeBetHe, Distributions

# ╔═╡ b554c5ab-61f4-4fa2-8010-93d41f0c8b90
using Graphs, LinearAlgebra, StatsBase, SparseArrays, Random, SpecialFunctions,KrylovKit,Plots

# ╔═╡ 586b400c-c012-11ee-0bbf-edef18950440
include("../src/header.jl")

# ╔═╡ be280717-64d7-4ae0-8ae5-83877f16dd03
function perturbed_observation!(Y::SparseMatrixCSC{ComplexF64,Int64},x::Vector{ComplexF64},eta::Float64,eps::Matrix{Float64})
	n = length(x)
	
	for j in 1:size(Y,2)
		for r in nzrange(Y,j)
			i = rowvals(Y)[r]
			Y[i,j] = (x[j]*conj(x[i])) * exp(im*eta*eps[i,j])
			Y[j,i] = conj(Y[i,j])
		end
	end
end

# ╔═╡ 2f06803f-e735-41f1-a258-9cb6ec6cc05d
function shape(l_e::Int64)
	if l_e == 1
		return :diamond
	else
		return :star5
	end
end

# ╔═╡ 3e789d80-8105-466c-95b6-3bce0c1d6553
begin
	n = UInt64(100); is_sbm = false

	#g = epsilon_graph(n,0.3,3) ; g_str = "eps"

	g = cycle_graph(n) ; g_str = "cycle"

	c = 20.
	cin = 19. ; cout = 1.
	theta = ones(n)
	theta ./= mean(theta)
	C = [cin cout; cout cin]*2
	l = create_label_vector(n,2,[0.5,0.5])
	A = adjacency_matrix_DCSBM(C,c,l,theta)
	#g = SimpleGraph{UInt64}(A); is_sbm = true ; g_str = "sbm"
	
	Y = ComplexF64.(adjacency_matrix(g))

	eps = ones(size(Y)) 

	combinatorial_laplacian = laplacian_matrix(g)

	x = unit_part.(randn(ComplexF64,n))

	Dx = Diagonal(conj.(x))

	is_connected(g)
end

# ╔═╡ eeb38be4-7052-45c8-b0fa-19a6cf9ebf85
# compute eigenvectors associated to the i-th smallest eigenvalues
begin
	i = 3
	eta_range = range(0,1,100)
	evL = Vector{Vector{ComplexF64}}(undef,length(eta_range))
	evLtheta = Vector{Vector{ComplexF64}}(undef,length(eta_range))
	aligned_evLtheta = Vector{Vector{ComplexF64}}(undef,length(eta_range))

	for eta_idx in 1:length(eta_range)
		eta = eta_range[eta_idx]
		evL[eta_idx] = eigen(Matrix(combinatorial_laplacian)).vectors[:,i]

		perturbed_observation!(Y,x,eta,eps)
		CG = ConnectionGraph(g,sparse(unit_part.(Y)),sparse(abs.(Y)))
		evLtheta[eta_idx] = eigen(Matrix(CG.L)).vectors[:,i] 

		aligned_evLtheta[eta_idx] = Dx*evLtheta[eta_idx]
	end
end

# ╔═╡ 75e76ed2-5ebe-4fa3-8585-22012cfe24fb
begin
	j1 = 12

	#pgfplotsx()
	gr()
	
	shape_vector = shape.(l)

	if is_sbm && i == 2
		scatter(real.(evL[j1]),imag.(evL[j1]),xlabel="REAL(z)",ylabel="IMAG(z)",label="vunderL",legend=:bottomright,markershape=shape_vector)
		scatter!(real.(evLtheta[j1]),imag.(evLtheta[j1]),label="vunderLtheta",markershape=shape_vector)
		scatter!(real.(aligned_evLtheta[j1]),imag.(aligned_evLtheta[j1]),label="Dx * vunderLtheta",markershape=shape_vector)
	
	else
		scatter(real.(evL[j1]),imag.(evL[j1]),xlabel="REAL(z)",ylabel="IMAG(z)",label="vunderL",legend=:bottomright)
		scatter!(real.(evLtheta[j1]),imag.(evLtheta[j1]),label="vunderLtheta")
		scatter!(real.(aligned_evLtheta[j1]),imag.(aligned_evLtheta[j1]),label="Dx * vunderLtheta")
	end
	#savefig("gsp/$(g_str)_j$(j1)_vp$(i).tikz")
end

# ╔═╡ af8aa574-af3b-42f1-99c2-3bfa79f18243
begin
	j2 = 75

	if is_sbm && i == 2
		scatter(real.(evL[j2]),imag.(evL[j2]),xlabel="REAL(z)",ylabel="IMAG(z)",label="vunderL",legend=:bottomright,markershape=shape_vector)
		scatter!(real.(evLtheta[j2]),imag.(evLtheta[j2]),label="vunderLtheta",markershape=shape_vector)
		scatter!(real.(aligned_evLtheta[j2]),imag.(aligned_evLtheta[j2]),label="Dx * vunderLtheta",markershape=shape_vector)
	else
		scatter(real.(evL[j2]),imag.(evL[j2]),xlabel="REAL(z)",ylabel="IMAG(z)",label="vunderL",legend=:bottomright)
		scatter!(real.(evLtheta[j2]),imag.(evLtheta[j2]),label="vunderLtheta")
		scatter!(real.(aligned_evLtheta[j2]),imag.(aligned_evLtheta[j2]),label="Dx * vunderLtheta")
	end
	#savefig("gsp/$(g_str)_j$(j2)_vp$(i).tikz")
end

# ╔═╡ 7c217dd7-7b9e-4f33-bc99-c886e4d1e4ee
begin
	gr()
	#pgfplotsx()
	
	plot(eta_range,aligned_l2_loss.(evL,aligned_evLtheta)/n,linewidth=2,xlabel="ETA",ylabel="NORMALIZED ERROR",legend=false,marker=2)
	vline!([pi/(2*n)],linewidth=2)
	vline!([eta_range[j1]],linewidth=2)
	vline!([eta_range[j2]],linewidth=2)
	#savefig("gsp/$(g_str)_vp$(i)_error.tikz")
end

# ╔═╡ 9e11a578-1d2e-4bb6-9678-518c328810e6
eta_range[2]

# ╔═╡ 02742e1b-6216-4b25-9fb4-862334687eda
pi/(2*n)

# ╔═╡ fd26432f-4007-474a-98d8-f7f5440e008a
eta_range[j1]

# ╔═╡ 1fc428f7-043c-4c36-b120-fea7be07d16b
eta_range[j2]

# ╔═╡ 8f2ad016-a500-4a4e-893b-ab0514fadd08
# marrant car le seuil d'incohérence n'est pas "strict" sur le cycle

# ╔═╡ 2945d267-be94-43af-b576-021de2217c75
# noter que le vecteur propre est défini à rotation près et que trouver la rotation optimale est un problème non trivial

# ╔═╡ 348240b7-ee24-47d2-869d-64d4878d802e
# pour tous les graphes faire 
# sbm i = 2 (pour montrer les communautés, loss linear in eta (not shown))
# eps i = 2 et 3 (faire celui-là d'abord)
# pour le cycle i = 2 (pour le seuil d'incohérence)

# ╔═╡ Cell order:
# ╠═b554c5ab-61f4-4fa2-8010-93d41f0c8b90
# ╠═6a19910f-0090-42ad-a51f-65e5cd20916e
# ╠═586b400c-c012-11ee-0bbf-edef18950440
# ╠═be280717-64d7-4ae0-8ae5-83877f16dd03
# ╠═2f06803f-e735-41f1-a258-9cb6ec6cc05d
# ╠═3e789d80-8105-466c-95b6-3bce0c1d6553
# ╠═eeb38be4-7052-45c8-b0fa-19a6cf9ebf85
# ╠═75e76ed2-5ebe-4fa3-8585-22012cfe24fb
# ╠═af8aa574-af3b-42f1-99c2-3bfa79f18243
# ╠═7c217dd7-7b9e-4f33-bc99-c886e4d1e4ee
# ╠═9e11a578-1d2e-4bb6-9678-518c328810e6
# ╠═02742e1b-6216-4b25-9fb4-862334687eda
# ╠═fd26432f-4007-474a-98d8-f7f5440e008a
# ╠═1fc428f7-043c-4c36-b120-fea7be07d16b
# ╠═8f2ad016-a500-4a4e-893b-ab0514fadd08
# ╠═2945d267-be94-43af-b576-021de2217c75
# ╠═348240b7-ee24-47d2-869d-64d4878d802e
