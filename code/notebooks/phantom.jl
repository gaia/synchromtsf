### A Pluto.jl notebook ###
# v0.19.26

using Markdown
using InteractiveUtils

# ╔═╡ 26e0539a-fcce-4109-86ce-1e074f2eb768
import Pkg; Pkg.add("BenchmarkTools")

# ╔═╡ f3a05b68-b5fc-11ee-116c-1341156066b9
using BenchmarkTools,Graphs,LinearAlgebra,StatsBase,SparseArrays,SpecialFunctions,Plots,Random, Distributions,KrylovKit,JLD,PGFPlotsX

# ╔═╡ 80a0fa5e-ade5-4b4c-8d45-c6a6f1ff5660
using ImageFeatures, TestImages, Images, ImageDraw, ImageView, CoordinateTransformations, Rotations

# ╔═╡ 00d90060-bf13-4ae3-a26a-b68643349112
include("../src/header.jl")

# ╔═╡ a51109d2-4f39-4ba8-9017-b4b42c99bf92
# outputs the (p,q)-th image moment of img
function moment(img::Matrix{Gray{Float64}},p::Int64,q::Int64)
	m = Float64(0)

	for x in 1:size(img)[1]
		for y in 1:size(img)[2]
			m += x^p * y^q * img[x,y]
		end
	end

	return m
end

# ╔═╡ 7366143a-098e-4fa7-82ae-8769f5541991
# outputs the (p,q)-th central image moment of img
function central_moment(img::Matrix{Gray{Float64}},p::Int64,q::Int64)
	m00 = moment(img,0,0)
	x_mean = moment(img,1,0)/m00
	y_mean = moment(img,0,1)/m00

	m = Float64(0)
	
	for x in 1:size(img)[1]
		for y in 1:size(img)[2]
			m += (x-x_mean)^p * (y-y_mean)^q * img[x,y]
		end
	end

	return m
end

# ╔═╡ 4326b731-ad6f-4a60-8adc-35adb9b27c90
# outputs the covariance matrice of img
function cov_matrix(img::Matrix{Gray{Float64}})
	cov = zeros(Float64,(2,2))
	
	m20 = central_moment(img,2,0)
	m02 = central_moment(img,0,2)
	m11 = central_moment(img,1,1)
	m00 = central_moment(img,0,0)
	
	cov[1,1] = m20/m00
	cov[2,2] = m02/m00
	cov[1,2] = m11/m00
	cov[2,1] = m11/m00
	return Hermitian(cov)
end

# ╔═╡ 773ad8bf-7b66-46f3-9a17-59279dc6a09b
# apply a rotation of specified angle to img
function rotate(img::Matrix{Gray{Float64}},angle::Float64)
	return imrotate(img,angle,axes(img),fillvalue=0)
end

# ╔═╡ a855d8d0-b3f9-4542-b55c-3aabb3be5ce8
# outputs the connection angle from img1 to img2
function registration_angle(img1::Matrix{Gray{Float64}},img2::Matrix{Gray{Float64}})
	cov1 = cov_matrix(img1)
	cov2 = cov_matrix(img2)
		
	cov_eig1 = eigen(cov1)
	cov_eig2 = eigen(cov2)

	angle1 = atan(cov_eig1.vectors[2,1],cov_eig1.vectors[1,1])
	angle2 = atan(cov_eig2.vectors[2,1],cov_eig2.vectors[1,1])

	return (angle1 - angle2) % (2*pi)
end

# ╔═╡ 6d5fcd6c-3247-496f-b75d-b834482795ca
function denoise(imgs::Vector{Matrix{Gray{Float64}}},angles::Vector{Float64})
	output = rotate(imgs[1],angles[1])

	for i in 2:length(imgs)
		output += rotate(imgs[i],angles[i])
	end

	return output/length(imgs)
end

# ╔═╡ 62ce2626-26b6-4d59-865b-0b36387f8bc0
# now the experiment

# ╔═╡ b6950f22-1f95-4f0a-aabf-e460ae9c4e18
ref_phantom = TestImages.shepp_logan(256)

# ╔═╡ e91a7b88-60fa-4eb2-8955-e7b83f9d199d
function sync_denoise_mtsf(SyncG::ConnectionGraph,init::Vector{ComplexF64},imgs::Vector{Matrix{Gray{Float64}}},sync_m::Int64,sync_k::Int64,sync_q::Float64)
	sync_q_vector = sync_q * ones(SyncG.n)
	
	phi = SmoothingMTSF(SyncG.n)
	buf = SamplingBuffer(SyncG.n)
	
	tmp_f = Vector{ComplexF64}(undef,SyncG.n)
	
	sync_eig = power_method(init,(x,y) -> _rb_smooth_with_MTSF!(SyncG,sync_q_vector,init,sync_m,phi,buf,y,tmp_f),nb_steps = sync_k)

	#sync_eig = power_method(init,(x,y) -> _rb_cv_smooth_with_MTSF!(SyncG,sync_q_vector,init,sync_m,phi,buf,y,tmp_f),nb_steps = sync_k)

	reconstruction_angles = angle.(sync_eig)
	
	denoised_phantom = denoise(imgs,reconstruction_angles)

	ra = registration_angle(denoised_phantom,ref_phantom)
	
	rotated_denoised_phantom = rotate(denoised_phantom,ra)
	
	if norm(ref_phantom - rotated_denoised_phantom) > norm(ref_phantom - rotate(denoised_phantom,ra+pi))
		rotated_denoised_phantom = rotate(denoised_phantom,ra+pi)
	end

	return rotated_denoised_phantom, sync_eig
end

# ╔═╡ 10043ddb-b80a-410e-81f1-d1c28b07fe43
function sync_denoise_exact(SyncG::ConnectionGraph,init::Vector{ComplexF64},imgs::Vector{Matrix{Gray{Float64}}})
	sync_eig = eigsolve(SyncG.L,1,:SR,maxiter=10000)[2][1]
	
	reconstruction_angles = angle.(sync_eig)
	
	denoised_phantom = denoise(imgs,reconstruction_angles)

	ra = registration_angle(denoised_phantom,ref_phantom)
	
	rotated_denoised_phantom = rotate(denoised_phantom,ra)
	
	if norm(ref_phantom - rotated_denoised_phantom) > norm(ref_phantom - rotate(denoised_phantom,ra+pi))
		rotated_denoised_phantom = rotate(denoised_phantom,ra+pi)
	end

	return rotated_denoised_phantom, sync_eig
end

# ╔═╡ a4bea15e-2453-4c2e-bab3-3f4a61d1b1ad
begin
	nb_images = UInt64(1000)

	imgs = Vector{Matrix{Gray{Float64}}}(undef,nb_images)

	av_deg = 5 # mean degree for Erdos-Renyi graph
	p = av_deg / nb_images
	underlying_graph = erdos_renyi(nb_images,p)
	while !is_connected(underlying_graph)
		global underlying_graph = erdos_renyi(nb_images,p)
	end
	
	obs_matrix = ComplexF64.(adjacency_matrix(underlying_graph))

	omega = unit_part.(randn(ComplexF64,nb_images))

	noise = Normal(0,5) # try going from 1 to 10 variance

	for k in 1:nb_images
		# we sample some angles uniformly in (-pi/2,pi/2)
		if real(omega[k]) < 0
			omega[k] *= -1
		end
		imgs[k] = rotate(ref_phantom,angle(omega[k])) + rand(noise,size(ref_phantom))
	end
	
	for e in edges(underlying_graph)
			i = e.src
			j = e.dst
			
			obs_matrix[i,j] = exp(im * -(registration_angle(imgs[i],imgs[j]) % pi))
			obs_matrix[j,i] = conj(obs_matrix[i,j])
	end

	SyncG = ConnectionGraph(underlying_graph,sparse(unit_part.(obs_matrix)),sparse(abs.(obs_matrix)))

	init = unit_part.(randn(ComplexF64,SyncG.n))
end

# ╔═╡ 8d93c235-723b-4755-9e99-83b49b438f12
imgs[1]

# ╔═╡ f2ce5801-0dc6-4eef-a7aa-2234ac69b932
begin
	sync_q = 0.001
	sync_k = 20
	sync_m = 10
	
	mtsf_denoised_phantom, mtsf_eig = sync_denoise_mtsf(SyncG,init,imgs,sync_m,sync_k,sync_q)

	mtsf_denoised_phantom
end

# ╔═╡ 7732a91d-9960-44b3-b2e5-d16106e434b2
begin
	exact_denoised_phantom,exact_eig = sync_denoise_exact(SyncG,init,imgs)

	exact_denoised_phantom
end

# ╔═╡ 18fd27da-07fc-4856-abb9-91849c0d995b
begin
	#save("../plots/phantom/phantom_v10/noisy_phantom.png",map(clamp01nan,imgs[1]))
	#save("../plots/phantom/phantom_v10/mtsf_denoised_phantom.png",map(clamp01nan,mtsf_denoised_phantom))
	#save("../plots/phantom/phantom_v10/exact_denoised_phantom.png",map(clamp01nan,exact_denoised_phantom))
	#save("../plots/phantom/phantom_v10/ref_pantom.png",ref_phantom)
end

# ╔═╡ 4c150c28-543c-4db4-ad6c-50f1861fb9ef
begin
	n_e = 100
	
	q = 0.001
	k = 20
	m_range = 1:10
	
	img_mtsf_errors = zeros(Float64,length(m_range))
	angle_mtsf_errors = zeros(Float64,length(m_range))
	img_exact_errors = zeros(Float64,length(m_range))
	angle_exact_errors = zeros(Float64,length(m_range))
	
	for e_idx in 1:n_e
		for m_idx in 1:length(m_range)
			m = m_range[m_idx]
			denoised_phantom, eig = sync_denoise_mtsf(SyncG,init,imgs,m,k,q)
			img_mtsf_errors[m_idx] += norm(denoised_phantom - ref_phantom)/n_e
			angle_mtsf_errors[m_idx] += aligned_l2_loss(unit_part.(eig),omega)/(SyncG.n * n_e)
		end
		denoised_phantom, eig = sync_denoise_exact(SyncG,init,imgs)
		img_exact_errors .= norm(denoised_phantom - ref_phantom)
		angle_exact_errors .= aligned_l2_loss(unit_part.(eig),omega)/SyncG.n
	end
end

# ╔═╡ bab7ca0e-2605-4463-8aab-b629e28cdb58
#save("../data/phantom/img_errors.jld","err",[img_mtsf_errors,img_exact_errors,angle_mtsf_errors,angle_exact_errors])

# ╔═╡ 900200a7-eb10-40a1-8249-08bd40c7cf79
begin
	gr()
	#pgfplotsx()
	plot(m_range,img_mtsf_errors,marker=4,markerstrokecolor=:auto,label="MTSF",linewidth=2)
	hline!(img_exact_errors,label="Exact",linewidth=2,seriescolor=palette(:default)[7])
	xlabel!("m")
	ylabel!("Image Reconstruction Error")
	ticks = collect(1:10)
	xticks!(ticks)
	#savefig("../plots/phantom/phantom_m10.tikz")
end

# ╔═╡ e708c9e3-e70f-4c46-85f4-a2e5539e8200
# écrire le fichier pour
# essayer pour différents m (et q ?) :
# itérer power method jusqu'à seuil de cv
# faire différentes valeurs de m (noter que ici pas weak-incoherent forcé)
# print la loss et les images en fonction du nb m de MTSF (av sur 10 rea)
# plot aussi loss synchro (aligned_L2_loss / n)
# prendre un petit graphe (n = 1000), et p = 5/n pour l'er
# -> similaire au régime du papier de cryo-em
# il faut aussi une ref pour la registration avec image moments
# faire m in 1:10 plutôt ?

# ╔═╡ b0940f85-8fcf-4d87-8106-66838bae771e
# illustrer pour var = 1 et 3 et 5
# -> comparer les courbes pour ces différentes valeurs

# ╔═╡ 2935c7e4-4700-48e5-a518-c5d96eb131eb
# quand m faible alors lissage peut être 0 à cause de unicycle ?

# ╔═╡ Cell order:
# ╠═26e0539a-fcce-4109-86ce-1e074f2eb768
# ╠═f3a05b68-b5fc-11ee-116c-1341156066b9
# ╠═80a0fa5e-ade5-4b4c-8d45-c6a6f1ff5660
# ╠═00d90060-bf13-4ae3-a26a-b68643349112
# ╠═a51109d2-4f39-4ba8-9017-b4b42c99bf92
# ╠═7366143a-098e-4fa7-82ae-8769f5541991
# ╠═4326b731-ad6f-4a60-8adc-35adb9b27c90
# ╠═773ad8bf-7b66-46f3-9a17-59279dc6a09b
# ╠═a855d8d0-b3f9-4542-b55c-3aabb3be5ce8
# ╠═6d5fcd6c-3247-496f-b75d-b834482795ca
# ╠═e91a7b88-60fa-4eb2-8955-e7b83f9d199d
# ╠═10043ddb-b80a-410e-81f1-d1c28b07fe43
# ╠═62ce2626-26b6-4d59-865b-0b36387f8bc0
# ╠═b6950f22-1f95-4f0a-aabf-e460ae9c4e18
# ╠═a4bea15e-2453-4c2e-bab3-3f4a61d1b1ad
# ╠═8d93c235-723b-4755-9e99-83b49b438f12
# ╠═f2ce5801-0dc6-4eef-a7aa-2234ac69b932
# ╠═7732a91d-9960-44b3-b2e5-d16106e434b2
# ╠═18fd27da-07fc-4856-abb9-91849c0d995b
# ╠═4c150c28-543c-4db4-ad6c-50f1861fb9ef
# ╠═bab7ca0e-2605-4463-8aab-b629e28cdb58
# ╠═900200a7-eb10-40a1-8249-08bd40c7cf79
# ╠═e708c9e3-e70f-4c46-85f4-a2e5539e8200
# ╠═b0940f85-8fcf-4d87-8106-66838bae771e
# ╠═2935c7e4-4700-48e5-a518-c5d96eb131eb
