### A Pluto.jl notebook ###
# v0.19.26

using Markdown
using InteractiveUtils

# ╔═╡ 6286a33a-5cd2-49e2-bc05-43c1e89307c6
using BenchmarkTools,Graphs,LinearAlgebra,StatsBase,SparseArrays,SpecialFunctions,Plots,Random,Pkg

# ╔═╡ b2458f18-0b5b-4d64-878c-8606cb9590de
Pkg.add(url="https://github.com/lorenzodallamico/CoDeBetHe")

# ╔═╡ 52c9eaba-68eb-4dce-b5fc-a7b7b211330e
using CoDeBetHe,Distributions

# ╔═╡ 8043c919-a474-4fe2-bb24-70fc061da740
using SNAPDatasets

# ╔═╡ a54e7abd-9fd6-4220-9e14-634a459a0410
include("../src/header.jl")

# ╔═╡ 1dec29c0-5300-11ee-26cb-f926135fa6b3
begin
	n = UInt64(10000)
	
	graph = epsilon_graph(n,0.1,3)

	#graph = SimpleGraph{UInt64}(loadsnap(:as_caida))
	
	c = 40.
    cin = 2*9. ; cout = 2*1.
    mixt = MixtureModel(Normal,[(50,20),(500,100),(10000,100),],[0.69,0.3,0.01])
	#theta = rand(mixt,n)
    theta = ones(n)
	theta ./= mean(theta)
    C = [cin cout;
    	cout cin]*2
    l = create_label_vector(n,2,[0.5,0.5])
    A = adjacency_matrix_DCSBM(C,c,l,theta)
	#graph = SimpleGraph{UInt64}(A)

	while !is_connected(graph)
		C = connected_components(graph)
		if length(C[length(C)]) == 1
			rem_vertex!(graph,C[length(C)][1])
		end
	end

	n = UInt64(nv(graph))

	adj = adjacency_matrix(graph)
	
	original_signal = unit_part.(randn(ComplexF64,n))
	Y = ComplexF64.(adjacency_matrix(graph))
	multiplicative_perturbation!(Y,original_signal)
	connection_matrix = unit_part.(Y)
	weight_matrix = abs.(Y)
	
	# build the resulting connection graph
	CG = ConnectionGraph(graph,sparse(connection_matrix),sparse(weight_matrix))
	
	q = mean(degree(graph))
	q *= 0.01
	q_vector = q*ones(n)	
end

# ╔═╡ effd2bd1-bd54-4b8b-8093-7a2ec2650d79
begin
	nb_samples = 20

	nb_evals = 5
	
	var_signal = randn(ComplexF64,CG.n)
	var_solution = chol\var_signal

	alpha_range = range(0.0001,2,100)

	rb_var = zeros(length(alpha_range))
	cv_var = zeros(length(alpha_range))
	
	for e_idx in 1:nb_evals
		for alpha_idx in 1:length(alpha_range)
			alpha = alpha_range[alpha_idx]
			tmp_rb = rb_smooth_with_MTSF(CG,q_vector,test_signal,nb_samples)
			rb_var[alpha_idx] += norm(var_solution - tmp_rb)

			dmax = maximum(CG.degree)
			dmin = minimum(CG.degree)

			D = Diagonal((CG.degree ./ ComplexF64(q_vector[1])) .+ ComplexF64(1.))
		
			tmp_cv = tmp_rb - inv(D) * (alpha * ((1/ComplexF64(q_vector[1])).*(CG.L*tmp_rb) .+ tmp_rb .- var_signal))
		
			cv_var[alpha_idx] += norm(var_solution - tmp_cv)
		end
	end
		
	rb_var ./= nb_evals
	cv_var ./= nb_evals
end

# ╔═╡ bb83195c-c532-4e3c-a5a3-4493f8a9041a
begin
	gr()
	#pgfplotsx()
	plot(alpha_range,rb_var,label="change_to_ftilde",xlabel="change_to_alpha",ylabel="change_to_average_error",linewidth=2,legend=:bottomright)
	plot!(alpha_range,cv_var,label="change_to_fhat",linewidth=2)
	#savefig("as_caida_var.tikz")
end

# ╔═╡ Cell order:
# ╠═6286a33a-5cd2-49e2-bc05-43c1e89307c6
# ╠═b2458f18-0b5b-4d64-878c-8606cb9590de
# ╠═52c9eaba-68eb-4dce-b5fc-a7b7b211330e
# ╠═8043c919-a474-4fe2-bb24-70fc061da740
# ╠═a54e7abd-9fd6-4220-9e14-634a459a0410
# ╠═1dec29c0-5300-11ee-26cb-f926135fa6b3
# ╠═effd2bd1-bd54-4b8b-8093-7a2ec2650d79
# ╠═bb83195c-c532-4e3c-a5a3-4493f8a9041a
