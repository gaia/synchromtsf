# outputs the id of the random successor of u in the random walk
function _random_successor(deg::UInt64)
	return rand(1:deg)
end

# outputs the id of the random successor of u in the random walk, for weighted graphs
# not efficient
function _random_successor(deg::UInt64,weight_list::Vector{Float64})
	return sample(1:deg,Weights(weight_list))
end

# outputs the id of the random successor of u in the random walk, for weighted graphs, using the alias method
function _random_successor(u::Node,deg::UInt64,abuf::AliasBuffer)
	i = rand(1:deg)
	rand(Float64) < abuf.prb[u][i] ? i : abuf.table[u][i]
end

"""
    sample_MTSF!(CG::ConnectionGraph,q_vector::Vector{Float64},phi::SmoothingMTSF,buf::SamplingBuffer)

Samples a MTSF from the `ConnectionGraph` `CG` using the node weights in `q_vector`. The resulting MTSF is stored in the `SmoothingMTSF` `phi`, and the `SamplingBuffer` `buf` stores additional data used by the algorithm.

In case a non-weakly-incoherent cycle is encountered, samples from the importance-sampling distribution.
"""
function sample_MTSF!(CG::ConnectionGraph,q_vector::Vector{Float64},phi::SmoothingMTSF,buf::SamplingBuffer)
	# variable initialization
	root = Node(0) # the root of the current path
	last_visited_node = Node(0) # the node last visited in the path

	# (re)-initializes the buffers
	init_MTSF!(phi,CG.n)
	init_buffer!(buf,CG.n)
	
	u = Node(0) # the current node
	tmp_next = UInt64(0) # the successor node in the random walk
	is_cycle = false # records if the current path is a cycle
	c = Float64(0) # used to store unicycle acceptance probabilities
	
	# counter variables
	ref_id = UInt64(1) # the current ID at the beginning of the for loop
	last_id = UInt64(1) # the last ID encountered
	cnt_value = UInt64(0) # stores the value of the current counter
	push!(buf.cnt_max,2*CG.n) # a bound on the maximum value of a new counter

	dq_vector = CG.degree .+ q_vector # pre-computed values for rooting rolls

	@inbounds for v in 1:CG.n
		u = v
		is_cycle = false
		last_visited_node = u

		ref_id = last_id

		while buf.state[u] != IN_TREE && buf.state[u] != IN_UNIC 
	
			# update counters
			buf.cnt[u].id = last_id
			cnt_value += UInt64(1)
			buf.cnt[u].value = cnt_value

			is_cycle = false

			# if u is selected as root
			if rand(Float64) < q_vector[u]/dq_vector[u]
				buf.state[u] = IN_TREE
				buf.next[u] = u
				last_visited_node = u

				root = u
				phi.root_of[u] = root
				phi.q_root_size[u] += q_vector[u]

			# if not, continue the walk
			else
				buf.next[u] = @inline _random_successor(CG.degree[u])
				tmp_next = CG.neighbors[u].list[buf.next[u]]
				
				# checks if a cycle has been created using the counter values
				is_cycle = ((ref_id <= buf.cnt[tmp_next].id) && (buf.cnt[tmp_next].value <= buf.cnt_max[buf.cnt[tmp_next].id]))

				# if it has, compute its holonomy
				if is_cycle
					cycle_holonomy = (buf.local_holonomy[u] + CG.neighbors[u].offset[buf.next[u]]) - buf.local_holonomy[tmp_next]
				end
			
				buf.local_holonomy[tmp_next] = buf.local_holonomy[u] + CG.neighbors[u].offset[buf.next[u]]
				u = tmp_next
				last_visited_node = u
			end

			if is_cycle
				c = cos(cycle_holonomy)
				if rand(Float64) <= 1-c
					buf.state[u] = IN_UNIC
					phi.importance_weight *= max(1,1-c)
				else
					buf.local_holonomy[u] = buf.local_holonomy[CG.neighbors[u].list[buf.next[u]]] - CG.neighbors[u].offset[buf.next[u]]
					# counter update
					cnt_value = buf.cnt[u].value - 1
					push!(buf.cnt_max,2*CG.n)
					buf.cnt_max[buf.cnt[u].id+1:last_id] .= 0
					buf.cnt_max[buf.cnt[u].id] = buf.cnt[u].value
					last_id += UInt64(1)
				end
			end
		end

		# create new counter
		last_id += UInt64(1)
		cnt_value = UInt64(0)
		push!(buf.cnt_max,2*CG.n)

		# add the path to the MTSF

		u = v 
		if phi.root_of[u] != u
			tmp_next = CG.neighbors[u].list[buf.next[u]] # the successor of u
		end

		# from v and until we reach a node already in the MTSF
		while buf.state[u] != IN_TREE && (buf.state[u] != IN_UNIC || buf.state[tmp_next] != IN_UNIC)
			# if u is in a rooted tree
			if buf.state[last_visited_node] != IN_UNIC
				buf.state[u] = IN_TREE
				root = phi.root_of[last_visited_node]
				phi.root_of[u] = root
				phi.q_root_size[root] += q_vector[u]
				phi.holonomy_to_root[u] = (phi.holonomy_to_root[last_visited_node] * exp(im*buf.local_holonomy[last_visited_node])) / exp(im*(buf.local_holonomy[u]))
			# else, record that u is in a unicycle
			else
				buf.state[u] = IN_UNIC
			end

			u = tmp_next
			if phi.root_of[u] != u
				tmp_next = CG.neighbors[u].list[buf.next[u]]
			end
		end
	end

	return;
end

"""
    sample_MTSF!(CG::WeightedConnectionGraph,q_vector::Vector{Float64},phi::SmoothingMTSF,buf::SamplingBuffer,abuf::AliasBuffer)

Samples a MTSF from the `WeightedConnectionGraph` `CG` using the node weights in `q_vector`. The resulting MTSF is stored in the `SmoothingMTSF` `phi`, and the `SamplingBuffer` `buf` stores additional data used by the algorithm.

In case a non-weakly-incoherent cycle is encountered, samples from the importance-sampling distribution.
"""
function sample_MTSF!(CG::WeightedConnectionGraph,q_vector::Vector{Float64},phi::SmoothingMTSF,buf::SamplingBuffer)
	# variable initialization
	root = Node(0) # the root of the current path
	last_visited_node = Node(0) # the node last visited in the path

	# (re)-initializes the buffers
	init_MTSF!(phi,CG.n)
	init_buffer!(buf,CG.n)
	
	u = Node(0) # the current node
	tmp_next = UInt64(0) # the successor node in the random walk
	is_cycle = false # records if the current path is a cycle
	c = Float64(0) # used to store unicycle acceptance probabilities
	
	# counter variables
	ref_id = UInt64(1) # the current ID at the beginning of the for loop
	last_id = UInt64(1) # the last ID encountered
	cnt_value = UInt64(0) # stores the value of the current counter
	push!(buf.cnt_max,2*CG.n) # a bound on the maximum value of a new counter
	
	dq_vector = CG.degree .+ q_vector # pre-computed values for rooting rolls

	@inbounds for v in 1:CG.n
		u = v
		is_cycle = false
		last_visited_node = u

		ref_id = last_id

		while buf.state[u] != IN_TREE && buf.state[u] != IN_UNIC 
	
			# update counters
			buf.cnt[u].id = last_id
			cnt_value += UInt64(1)
			buf.cnt[u].value = cnt_value

			is_cycle = false

			# if u is selected as root
			if rand(Float64) < q_vector[u]/dq_vector[u]
				buf.state[u] = IN_TREE
				buf.next[u] = u
				last_visited_node = u

				root = u
				phi.root_of[u] = root
				phi.q_root_size[u] += q_vector[u]

			# if not, continue the walk
			else
				buf.next[u] = @inline _random_successor(CG.c_degree[u],CG.neighbors[u].weight)
				tmp_next = CG.neighbors[u].list[buf.next[u]]
				
				# checks if a cycle has been created using the counter values
				is_cycle = ((ref_id <= buf.cnt[tmp_next].id) && (buf.cnt[tmp_next].value <= buf.cnt_max[buf.cnt[tmp_next].id]))

				# if it has, compute its holonomy
				if is_cycle
					cycle_holonomy = (buf.local_holonomy[u] + CG.neighbors[u].offset[buf.next[u]]) - buf.local_holonomy[tmp_next]
				end
			
				buf.local_holonomy[tmp_next] = buf.local_holonomy[u] + CG.neighbors[u].offset[buf.next[u]]
				u = tmp_next
				last_visited_node = u
			end

			if is_cycle
				c = cos(cycle_holonomy)
				if rand(Float64) <= 1-c
					buf.state[u] = IN_UNIC
					phi.importance_weight *= max(1,1-c)
				else
					buf.local_holonomy[u] = buf.local_holonomy[CG.neighbors[u].list[buf.next[u]]] - CG.neighbors[u].offset[buf.next[u]]
					# counter update
					cnt_value = buf.cnt[u].value - 1
					push!(buf.cnt_max,2*CG.n)
					buf.cnt_max[buf.cnt[u].id+1:last_id] .= 0
					buf.cnt_max[buf.cnt[u].id] = buf.cnt[u].value
					last_id += UInt64(1)
				end
			end
		end

		# create new counter
		last_id += UInt64(1)
		cnt_value = UInt64(0)
		push!(buf.cnt_max,2*CG.n)

		# add the path to the MTSF

		u = v 
		if phi.root_of[u] != u
			tmp_next = CG.neighbors[u].list[buf.next[u]] # the successor of u
		end

		# from v and until we reach a node already in the MTSF
		while buf.state[u] != IN_TREE && (buf.state[u] != IN_UNIC || buf.state[tmp_next] != IN_UNIC)
			# if u is in a rooted tree
			if buf.state[last_visited_node] != IN_UNIC
				buf.state[u] = IN_TREE
				root = phi.root_of[last_visited_node]
				phi.root_of[u] = root
				phi.q_root_size[root] += q_vector[u]
				phi.holonomy_to_root[u] = (phi.holonomy_to_root[last_visited_node] * exp(im*buf.local_holonomy[last_visited_node])) / exp(im*(buf.local_holonomy[u]))
			# else, record that u is in a unicycle
			else
				buf.state[u] = IN_UNIC
			end

			u = tmp_next
			if phi.root_of[u] != u
				tmp_next = CG.neighbors[u].list[buf.next[u]]
			end
		end
	end

	return;
end

"""
    sample_MTSF!(CG::WeightedConnectionGraph,q_vector::Vector{Float64},phi::SmoothingMTSF,buf::SamplingBuffer,abuf::AliasBuffer)

Samples a MTSF from the `WeightedConnectionGraph` `CG` using the node weights in `q_vector`. The resulting MTSF is stored in the `SmoothingMTSF` `phi`, and the `SamplingBuffer` `buf` and the `AliasBuffer` `abuf` store additional data used by the algorithm. Uses the alias method for neighbor computation.

In case a non-weakly-incoherent cycle is encountered, samples from the importance-sampling distribution.
"""
function sample_MTSF!(CG::WeightedConnectionGraph,q_vector::Vector{Float64},phi::SmoothingMTSF,buf::SamplingBuffer,abuf::AliasBuffer)
	# variable initialization
	root = Node(0) # the root of the current path
	last_visited_node = Node(0) # the node last visited in the path

	# (re)-initializes the buffers
	init_MTSF!(phi,CG.n)
	init_buffer!(buf,CG.n)
	
	u = Node(0) # the current node
	tmp_next = UInt64(0) # the successor node in the random walk
	is_cycle = false # records if the current path is a cycle
	c = Float64(0) # used to store unicycle acceptance probabilities
	
	# counter variables
	ref_id = UInt64(1) # the current ID at the beginning of the for loop
	last_id = UInt64(1) # the last ID encountered
	cnt_value = UInt64(0) # stores the value of the current counter
	push!(buf.cnt_max,2*CG.n) # a bound on the maximum value of a new counter
	
	dq_vector = CG.degree .+ q_vector # pre-computed values for rooting rolls

	@inbounds for v in 1:CG.n
		truc = 0
		u = v
		is_cycle = false
		last_visited_node = u

		ref_id = last_id

		while buf.state[u] != IN_TREE && buf.state[u] != IN_UNIC 
	
			# update counters
			buf.cnt[u].id = last_id
			cnt_value += UInt64(1)
			buf.cnt[u].value = cnt_value

			is_cycle = false

			# if u is selected as root
			if rand(Float64) < q_vector[u]/dq_vector[u]
				buf.state[u] = IN_TREE
				buf.next[u] = u
				last_visited_node = u

				root = u
				phi.root_of[u] = root
				phi.q_root_size[u] += q_vector[u]

			# if not, continue the walk
			else
				buf.next[u] = @inline _random_successor(u,CG.c_degree[u],abuf)
				tmp_next = CG.neighbors[u].list[buf.next[u]]
				
				# checks if a cycle has been created using the counter values
				is_cycle = ((ref_id <= buf.cnt[tmp_next].id) && (buf.cnt[tmp_next].value <= buf.cnt_max[buf.cnt[tmp_next].id]))

				# if it has, compute its holonomy
				if is_cycle
					cycle_holonomy = (buf.local_holonomy[u] + CG.neighbors[u].offset[buf.next[u]]) - buf.local_holonomy[tmp_next]
				end
			
				buf.local_holonomy[tmp_next] = buf.local_holonomy[u] + CG.neighbors[u].offset[buf.next[u]]
				u = tmp_next
				last_visited_node = u
			end

			if is_cycle
				c = cos(cycle_holonomy)
				if rand(Float64) <= 1-c
					buf.state[u] = IN_UNIC
					phi.importance_weight *= max(1,1-c)
				else
					buf.local_holonomy[u] = buf.local_holonomy[CG.neighbors[u].list[buf.next[u]]] - CG.neighbors[u].offset[buf.next[u]]
					# counter update
					cnt_value = buf.cnt[u].value - 1
					push!(buf.cnt_max,2*CG.n)
					buf.cnt_max[buf.cnt[u].id+1:last_id] .= 0
					buf.cnt_max[buf.cnt[u].id] = buf.cnt[u].value
					last_id += UInt64(1)
				end
			end
		end

		# create new counter
		last_id += UInt64(1)
		cnt_value = UInt64(0)
		push!(buf.cnt_max,2*CG.n)

		# add the path to the MTSF

		u = v 
		if phi.root_of[u] != u
			tmp_next = CG.neighbors[u].list[buf.next[u]] # the successor of u
		end

		# from v and until we reach a node already in the MTSF
		while buf.state[u] != IN_TREE && (buf.state[u] != IN_UNIC || buf.state[tmp_next] != IN_UNIC)
			# if u is in a rooted tree
			if buf.state[last_visited_node] != IN_UNIC
				buf.state[u] = IN_TREE
				root = phi.root_of[last_visited_node]
				phi.root_of[u] = root
				phi.q_root_size[root] += q_vector[u]
				phi.holonomy_to_root[u] = (phi.holonomy_to_root[last_visited_node] * exp(im*buf.local_holonomy[last_visited_node])) / exp(im*(buf.local_holonomy[u]))
			# else, record that u is in a unicycle
			else
				buf.state[u] = IN_UNIC
			end

			u = tmp_next
			if phi.root_of[u] != u
				tmp_next = CG.neighbors[u].list[buf.next[u]]
			end
		end
	end

	return;
end

