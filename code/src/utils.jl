# f and g are assumed to be normalized
"""
    aligned_l2_loss(f::Vector{ComplexF64},g::Vector{ComplexF64})

Outputs the minimal norm of (`f` - r `g`) over all rotations r, for normalized vectors.
"""
function aligned_l2_loss(f::Vector{ComplexF64},g::Vector{ComplexF64})
	return sqrt(abs(2(1 - abs(transpose(conj.(f))*g))))
end

"""
    aligned_l2_loss(f::Vector{ComplexF64},g::Vector{ComplexF64})

Outputs the minimal norm of (`f` - r `g`) over all rotations r, for entry-wise normalized vectors.
"""
function unitary_aligned_l2_loss(f::Vector{ComplexF64},g::Vector{ComplexF64})
	return sqrt(abs(2*(length(f) - abs(transpose(conj.(f))*g))))
end
	
function unit_part(c::ComplexF64)
	if c != 0
		return c/abs(c)
	else
		return ComplexF64(0)
	end
end

"""
    epsilon_graph(n::UInt64,eps::Float64,d::Int64)

Outputs an ε-graph with `n` nodes sampled in [0,1]^`d`, and an edge if two nodes are less than `eps` apart.
"""
function epsilon_graph(n::UInt64,eps::Float64,d::Int64)
	g = Graphs.euclidean_graph(Int(n),d,cutoff=eps)[1]
	return SimpleGraph{UInt64}(g)
end

"""
    random_spanning_tree(CG::ConnectionGraph; root::Node=Node(1))

Outputs a uniform spanning tree in `CG`, with prescribed `root`.
"""
function random_spanning_tree(CG::ConnectionGraph; root::Node=Node(1))
	in_tree = falses(CG.n)
	next = zeros(Node,CG.n)
	in_tree[root] = true

	for v in 1:CG.n
		u = v
		while !in_tree[u]
			nn = outneighbors(CG.g,u)
			next[u] = rand(nn)
			u = next[u]
		end

		u = v
		while !in_tree[u]
			in_tree[u] = true
			u = next[u]
		end
	end
	
	tree = SimpleGraph{Node}(CG.n)
	for v in 1:CG.n
		next[v] > 0 && add_edge!(tree,v,next[v])
	end
	
	return tree
end

# outputs a minimum spanning tree of g, using Kruskal's algorithm
# uses abs(cos(\theta_{i,j})) as edge-weights
"""
    min_spanning_tree(CG::ConnectionGraph)

Outputs a minimum spanning tree in `CG`, with abs(cos(theta_{i,j})) as edge-weights.
"""
function min_spanning_tree(CG::ConnectionGraph)
	edges = kruskal_mst(CG.g,sparse(abs.(real.(CG.C))))
	tree = SimpleGraph{Node}(CG.n)
	for e in edges
		add_edge!(tree,e.src,e.dst)
	end
	
	return tree
end

"""
    ust_synchronization(CG::ConnectionGraph,root::Node=Node(1))

Samples a UST and performs a naive synchronization.
"""
function ust_synchronization(CG::ConnectionGraph,root::Node=Node(1))
	sync_res = ones(ComplexF64,CG.n)	

	tree = random_spanning_tree(CG)

	# convenient way to obtain paths from the root to each node
	# could also be recorded while sampling
	out = dijkstra_shortest_paths(tree,root)
	path_list = enumerate_paths(out)

	for p_idx in 1:length(path_list)
		p = path_list[p_idx]
		if p != []
			end_point = p[length(p)]
			for e_idx in 2:length(p)
				sync_res[end_point] *= CG.C[p[e_idx-1],p[e_idx]]
			end
		end
	end

	return sync_res
end

"""
    mst_synchronization(CG::ConnectionGraph,root::Node=Node(1))

Samples a MST and performs a naive synchronization.
"""
function mst_synchronization(CG::ConnectionGraph,root::Node=Node(1))
	sync_res = ones(ComplexF64,CG.n)	

	tree = min_spanning_tree(CG)

	# convenient way to obtain paths from the root to each node
	# could also be recorded while sampling
	out = dijkstra_shortest_paths(tree,root)
	path_list = enumerate_paths(out)

	for p_idx in 1:length(path_list)
		p = path_list[p_idx]
		if p != []
			end_point = p[length(p)]
			for e_idx in 2:length(p)
				sync_res[end_point] *= CG.C[p[e_idx-1],p[e_idx]]
			end
		end
	end

	return sync_res
end

"""
    multiplicative_perturbation!(obs::SparseMatrixCSC{ComplexF64,Int64},f::Vector{ComplexF64},a::Float64)

Fills the nonzero entries of `obs` with angular offsets measured from `f` perturbed by uniformly distributed noise in [-`a`,`a`]. `a` defaults to 1.
"""
function multiplicative_perturbation!(obs::SparseMatrixCSC{ComplexF64,Int64},f::Vector{ComplexF64};a::Float64=Inf)
	n = length(f)

	if a == Inf
		gamma = pi/(2*n)
	else
		gamma = a
	end

	for j in 1:size(obs,2)
		for r in nzrange(obs,j)
			i = rowvals(obs)[r]
			obs[i,j] = (f[j]*conj(f[i]) * exp(im*(rand()-.5)*2*gamma))
			obs[j,i] = conj(obs[i,j])
		end
	end
end
