# Regular estimator

"""
    _propagate_in_MTSF!(CG::ConnectionGraph,phi::SmoothingMTSF,g::Vector{ComplexF64},f::Vector{ComplexF64})

Propagates the values of the signal `g` at the roots of the MTSF described by the `SmoothingMTSF` `phi` to all the node in the `ConnectionGraph` `CG`, the result is stored in `f`
"""
function _propagate_in_MTSF!(phi::SmoothingMTSF,g::Vector{ComplexF64},f::Vector{ComplexF64},n::UInt64)
	@inbounds @simd for v in 1:n
		f[v] += phi.importance_weight * (conj(phi.holonomy_to_root[v]) * g[phi.root_of[v]] * ComplexF64(!iszero(phi.root_of[v])))
	end
end

# Smoothes the signal `g` on the nodes of the `ConnectionGraph` `CG`, with respect to the parameters in `q_vector`, using `nb_MTSF` MTSFs. The result is stored in `f`.
function _smooth_with_MTSF!(CG::ConnectionGraph,q_vector::Vector{Float64},g::Vector{ComplexF64},nb_MTSF::Int64,phi::SmoothingMTSF,buf::SamplingBuffer,f::Vector{ComplexF64})
	f .= ComplexF64(0)

	@inbounds for i in 1:nb_MTSF
		sample_MTSF!(CG,q_vector,phi,buf)
		@inline _propagate_in_MTSF!(phi,g,f,CG.n)
	end

	f ./= nb_MTSF
	return
end

# Smoothes the signal `g` on the nodes of the `WeightedConnectionGraph` `CG`, with respect to the parameters in `q_vector`, using `nb_MTSF` MTSFs. The result is stored in `f`.
function _smooth_with_MTSF!(CG::WeightedConnectionGraph,q_vector::Vector{Float64},g::Vector{ComplexF64},nb_MTSF::Int64,phi::SmoothingMTSF,buf::SamplingBuffer,f::Vector{ComplexF64})
	f .= ComplexF64(0)

	@inbounds for i in 1:nb_MTSF
		sample_MTSF!(CG,q_vector,phi,buf)
		@inline _propagate_in_MTSF!(phi,g,f,CG.n)
	end

	f ./= nb_MTSF
	return
end

# Smoothes the signal `g` on the nodes of the `WeightedConnectionGraph` `CG`, with respect to the parameters in `q_vector`, using `nb_MTSF` MTSFs. The result is stored in `f`. Uses the alias method.
function _smooth_with_MTSF!(CG::WeightedConnectionGraph,q_vector::Vector{Float64},g::Vector{ComplexF64},nb_MTSF::Int64,phi::SmoothingMTSF,buf::SamplingBuffer,abuf::AliasBuffer,f::Vector{ComplexF64})
	f .= ComplexF64(0)

	@inbounds for i in 1:nb_MTSF
		sample_MTSF!(CG,q_vector,phi,buf,abuf)
		@inline _propagate_in_MTSF!(phi,g,f,CG.n)
	end

	f ./= nb_MTSF
	return
end

"""
    smooth_with_MTSF(CG::ConnectionGraph,q_vector::Vector{Float64},g::Vector{ComplexF64},nb_MTSF::Int64)

Smoothes the signal `g`on the nodes of the (eventually weighted) `ConnectionGraph` `CG`, with respect to the parameters in the `q_vector`, using `nb_MTSF` MTSFs. Use of the alias method for sampling in weighted graphs can be turned off by setting use_alias = false.
"""
function smooth_with_MTSF(CG::Union{ConnectionGraph,WeightedConnectionGraph},q_vector::Vector{Float64},g::Vector{ComplexF64},nb_MTSF::Int64;use_alias::Bool=true)
	phi = SmoothingMTSF(CG.n)
	buf = SamplingBuffer(CG.n)
	
	f = Vector{ComplexF64}(undef,CG.n)

	if typeof(CG) == WeightedConnectionGraph && use_alias
		abuf = AliasBuffer(CG)
		_smooth_with_MTSF!(CG,q_vector,g,nb_MTSF,phi,buf,abuf,f)
	else
		_smooth_with_MTSF!(CG,q_vector,g,nb_MTSF,phi,buf,f)
	end

	return f
end

# Rao-Blackwell estimator

"""
    propagate_average_in_MTSF!(CG::ConnectionGraph,phi::SmoothingMTSF,g::Vector{ComplexF64},f::Vector{ComplexF64})

Propagates and sums the values of the signal `g` from the nodes in the `ConnectionGraph` `CG` to their roots along the MTSF described by the `SmoothingMTSF` `phi`, before propagating them back. The result is stored in `f`.
"""
function _propagate_average_in_MTSF!(phi::SmoothingMTSF,g::Vector{ComplexF64},f::Vector{ComplexF64},n::UInt64,q_vector::Vector{Float64})
	@inbounds @simd for v in 1:n
		f[phi.root_of[v]] += (phi.importance_weight * q_vector[v]) * ((phi.holonomy_to_root[v] * g[v] * ComplexF64(!iszero(phi.root_of[v]))) / phi.q_root_size[phi.root_of[v]]) 
	end
	
	@inbounds for v in 1:n
		if phi.root_of[v] != 0 && v != phi.root_of[v]
			f[v] = (conj(phi.holonomy_to_root[v]) * f[phi.root_of[v]]) 
		end
	end
end

# Smoothes the signal `g` on the nodes of the `ConnectionGraph` `CG`, with respect to the parameters in `q_vector`, using `nb_MTSF` MTSFs. The result is stored in `f` and `tmp_f` is used as a buffer. This is a Rao-Blackwellized version of the smoothing performed by `smooth_with_MTSF!`.
function _rb_smooth_with_MTSF!(CG::ConnectionGraph,q_vector::Vector{Float64},g::Vector{ComplexF64},nb_MTSF::Int64,phi::SmoothingMTSF,buf::SamplingBuffer,f::Vector{ComplexF64},tmp_f::Vector{ComplexF64})
	f .= ComplexF64(0)
	tmp_f .= ComplexF64(0)

	@inbounds for i in 1:nb_MTSF
		sample_MTSF!(CG,q_vector,phi,buf)
		@inline _propagate_average_in_MTSF!(phi,g,tmp_f,CG.n,q_vector)
		f .+= tmp_f
		tmp_f .= ComplexF64(0)
	end
	
	f ./= nb_MTSF
	return
end

# Smoothes the signal `g` on the nodes of the `WeightedConnectionGraph` `CG`, with respect to the parameters in `q_vector`, using `nb_MTSF` MTSFs. The result is stored in `f` and `tmp_f` is used as a buffer. This is a Rao-Blackwellized version of the smoothing performed by `smooth_with_MTSF!`.
function _rb_smooth_with_MTSF!(CG::WeightedConnectionGraph,q_vector::Vector{Float64},g::Vector{ComplexF64},nb_MTSF::Int64,phi::SmoothingMTSF,buf::SamplingBuffer,f::Vector{ComplexF64},tmp_f::Vector{ComplexF64})
	f .= ComplexF64(0)
	tmp_f .= ComplexF64(0)

	@inbounds for i in 1:nb_MTSF
		sample_MTSF!(CG,q_vector,phi,buf)
		@inline _propagate_average_in_MTSF!(phi,g,tmp_f,CG.n,q_vector)
		f .+= tmp_f
		tmp_f .= ComplexF64(0)
	end
	
	f ./= nb_MTSF
	return
end

# Smoothes the signal `g` on the nodes of the `WeightedConnectionGraph` `CG`, with respect to the parameters in `q_vector`, using `nb_MTSF` MTSFs. The result is stored in `f` and `tmp_f` is used as a buffer. This is a Rao-Blackwellized version of the smoothing performed by `smooth_with_MTSF!`. Uses the alias method.
function _rb_smooth_with_MTSF!(CG::WeightedConnectionGraph,q_vector::Vector{Float64},g::Vector{ComplexF64},nb_MTSF::Int64,phi::SmoothingMTSF,buf::SamplingBuffer,abuf::AliasBuffer,f::Vector{ComplexF64},tmp_f::Vector{ComplexF64})
	f .= ComplexF64(0)
	tmp_f .= ComplexF64(0)

	@inbounds for i in 1:nb_MTSF
		sample_MTSF!(CG,q_vector,phi,buf,abuf)
		@inline _propagate_average_in_MTSF!(phi,g,tmp_f,CG.n,q_vector)
		f .+= tmp_f
		tmp_f .= ComplexF64(0)
	end
	
	f ./= nb_MTSF
	return
end

"""
    rb_smooth_with_MTSF(CG::ConnectionGraph,q_vector::Vector{Float64},g::Vector{ComplexF64},nb_MTSF::Int64)

Smoothes the signal `g` on the nodes of the (eventually weighted) `ConnectionGraph` `CG`, with respect to the parameters in `q_vector`, using `nb_MTSF` MTSFs. Use of the alias method for sampling in weighted graphs can be turned off by setting use_alias = false.

This is a Rao-Blackwellized version of the smoothing performed by `smooth_with_MTSF`.
"""
function rb_smooth_with_MTSF(CG::Union{ConnectionGraph,WeightedConnectionGraph},q_vector::Vector{Float64},g::Vector{ComplexF64},nb_MTSF::Int64;use_alias::Bool=true)
	phi = SmoothingMTSF(CG.n)
	buf = SamplingBuffer(CG.n)

	f = Vector{ComplexF64}(undef,CG.n)
	tmp_f = Vector{ComplexF64}(undef,CG.n)

	if typeof(CG) == WeightedConnectionGraph && use_alias
		abuf = AliasBuffer(CG)
		_rb_smooth_with_MTSF!(CG,q_vector,g,nb_MTSF,phi,buf,abuf,f,tmp_f)
	else
		_rb_smooth_with_MTSF!(CG,q_vector,g,nb_MTSF,phi,buf,f,tmp_f)
	end

	return f
end

# Rao-Blackwell estimator with Control Variates

# Smoothes the signal `g` on the nodes of the `ConnectionGraph` `CG`, with respect to the parameters in `q_vector`, using `nb_MTSF` MTSFs. The result is stored in `f` and `tmp_f` is used as a buffer. This is a "control-variates" enhanced version of the smoothing performed by `rb_smooth_with_MTSF!`.
function _rb_cv_smooth_with_MTSF!(CG::ConnectionGraph,q_vector::Vector{Float64},g::Vector{ComplexF64},nb_MTSF::Int64,phi::SmoothingMTSF,buf::SamplingBuffer,f::Vector{ComplexF64},tmp_f::Vector{ComplexF64})
	f .= ComplexF64(0)
	tmp_f .= ComplexF64(0)

	@inbounds for i in 1:nb_MTSF
		sample_MTSF!(CG,q_vector,phi,buf)
		@inline _propagate_average_in_MTSF!(phi,g,tmp_f,CG.n,q_vector)
		f .+= tmp_f
		tmp_f .= ComplexF64(0)
	end
	
	f ./= nb_MTSF

	# we assume uniform q
	
	#D = Diagonal((CG.degree ./ ComplexF64(q_vector[1])) .+ ComplexF64(1.))
	#f .-= inv(D) * ((1/ComplexF64(q_vector[1])).*(CG.L*f) .+ f .- g)
	
	D = Diagonal(CG.degree ./ ComplexF64.(q_vector) .+ ComplexF64(1.))
	Q = Diagonal(ComplexF64.(q_vector))
	f .-= inv(D) * (inv(Q)*(CG.L*f) .+ f .- g)
	
	return
end

# Smoothes the signal `g` on the nodes of the `WeightedConnectionGraph` `CG`, with respect to the parameters in `q_vector`, using `nb_MTSF` MTSFs. The result is stored in `f` and `tmp_f` is used as a buffer. This is a "control-variates" enhanced version of the smoothing performed by `rb_smooth_with_MTSF!`. 
function _rb_cv_smooth_with_MTSF!(CG::WeightedConnectionGraph,q_vector::Vector{Float64},g::Vector{ComplexF64},nb_MTSF::Int64,phi::SmoothingMTSF,buf::SamplingBuffer,f::Vector{ComplexF64},tmp_f::Vector{ComplexF64})
	f .= ComplexF64(0)
	tmp_f .= ComplexF64(0)

	@inbounds for i in 1:nb_MTSF
		sample_MTSF!(CG,q_vector,phi,buf)
		@inline _propagate_average_in_MTSF!(phi,g,tmp_f,CG.n,q_vector)
		f .+= tmp_f
		tmp_f .= ComplexF64(0)
	end
	
	f ./= nb_MTSF

	#D = Diagonal((CG.degree ./ ComplexF64(q_vector[1])) .+ ComplexF64(1.))
	#f .-= inv(D) * ((1/ComplexF64(q_vector[1])).*(CG.L*f) .+ f .- g)
	
	D = Diagonal(CG.degree ./ ComplexF64.(q_vector) .+ ComplexF64(1.))
	Q = Diagonal(ComplexF64.(q_vector))
	f .-= inv(D) * (inv(Q)*(CG.L*f) .+ f .- g)
	
	return
end

# Smoothes the signal `g` on the nodes of the `WeightedConnectionGraph` `CG`, with respect to the parameters in `q_vector`, using `nb_MTSF` MTSFs. The result is stored in `f` and `tmp_f` is used as a buffer. This is a "control-variates" enhanced version of the smoothing performed by `rb_smooth_with_MTSF!`. Uses the alias method.
function _rb_cv_smooth_with_MTSF!(CG::WeightedConnectionGraph,q_vector::Vector{Float64},g::Vector{ComplexF64},nb_MTSF::Int64,phi::SmoothingMTSF,buf::SamplingBuffer,abuf::AliasBuffer,f::Vector{ComplexF64},tmp_f::Vector{ComplexF64})
	f .= ComplexF64(0)
	tmp_f .= ComplexF64(0)

	@inbounds for i in 1:nb_MTSF
		sample_MTSF!(CG,q_vector,phi,buf,abuf)
		@inline _propagate_average_in_MTSF!(phi,g,tmp_f,CG.n,q_vector)
		f .+= tmp_f
		tmp_f .= ComplexF64(0)
	end
	
	f ./= nb_MTSF

	#D = Diagonal((CG.degree ./ ComplexF64(q_vector[1])) .+ ComplexF64(1.))
	#f .-= inv(D) * ((1/ComplexF64(q_vector[1])).*(CG.L*f) .+ f .- g)
	
	D = Diagonal(CG.degree ./ ComplexF64.(q_vector) .+ ComplexF64(1.))
	Q = Diagonal(ComplexF64.(q_vector))
	f .-= inv(D) * (inv(Q)*(CG.L*f) .+ f .- g)
	
	return
end

"""
    rb_cv_smooth_with_MTSF(CG::ConnectionGraph,q_vector::Vector{Float64},g::Vector{ComplexF64},nb_MTSF::Int64)

Smoothes the signal `g` on the nodes of the (eventually weighted) `ConnectionGraph` `CG`, with respect to the parameters in `q_vector`, using `nb_MTSF` MTSFs. Use of the alias method for sampling in weighted graphs can be turned off by setting use_alias = false.

This is a "control-variates" enhanced version of the smoothing performed by `rb_smooth_with_MTSF`.
"""
function rb_cv_smooth_with_MTSF(CG::Union{ConnectionGraph,WeightedConnectionGraph},q_vector::Vector{Float64},g::Vector{ComplexF64},nb_MTSF::Int64;use_alias::Bool=true)
	phi = SmoothingMTSF(CG.n)
	buf = SamplingBuffer(CG.n)

	f = Vector{ComplexF64}(undef,CG.n)
	tmp_f = Vector{ComplexF64}(undef,CG.n)

	if typeof(CG) == WeightedConnectionGraph && use_alias
		abuf = AliasBuffer(CG)
		_rb_cv_smooth_with_MTSF!(CG,q_vector,g,nb_MTSF,phi,buf,abuf,f,tmp_f)
	else
		_rb_cv_smooth_with_MTSF!(CG,q_vector,g,nb_MTSF,phi,buf,f,tmp_f)
	end

	return f
end
