# Power Method
# f a linear operator

function _power_method_iteration!(input::Vector{ComplexF64},output::Vector{ComplexF64},f::Function,nb_steps::Int64,epsilon::Float64)
	@assert nb_steps > 0 && epsilon > 0

	i = Int64(1)
	delta = Float64(epsilon + 1.)

	while i <= nb_steps && delta > epsilon
		f(input,output) # acts on output
		if norm(output) != 0 # in case we sample a unique unicycle
			output ./= norm(output)
		end
		i += 1
		delta = Float64(norm(input - output))
		input .= output
	end
end
		
function power_method(g::Vector{ComplexF64},f::Function;nb_steps::Int64=1000,epsilon::Float64=1e-12)
	input = copy(g)
	output = Vector{ComplexF64}(undef,length(input))
	_power_method_iteration!(input,output,f,nb_steps,epsilon) 
	return output
end

# Projected Power Method

function _projected_power_method_iteration!(input::Vector{ComplexF64},output::Vector{ComplexF64},f::Function,nb_steps::Int64,epsilon::Float64)
	@assert nb_steps > 0 && epsilon > 0

	i = Int64(1)
	delta = Float64(epsilon + 1.)

	while i <= nb_steps && delta > epsilon
		f(input,output) # acts on output
		output ./= abs.(output) # can misbehave for unicycles
		i += 1
		delta = Float64(norm(input - output))
		input .= output
	end
end
		
function projected_power_method(g::Vector{ComplexF64},f::Function;nb_steps::Int64=1000,epsilon::Float64=1e-12)
	input = copy(g)
	output = Vector{ComplexF64}(undef,length(input))
	_projected_power_method_iteration!(input,output,f,nb_steps,epsilon) 
	return output
end

