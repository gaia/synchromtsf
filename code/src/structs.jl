# Aliases

# a Node is represented as UInt64
"""
    Node

We represent the nodes of our graphs as unsigned integers.
"""
global const Node = UInt64

# may benefit from bitstring representation
global const IN_TREE::UInt8 = 1
global const IN_UNIC::UInt8 = 2

# Structures

"""
	SmoothingMTSF

Contains the data, describing a MTSF, necessary to perform the smoothing operation.

`root_of` a vector describing the root of each node (0 if the node is in a unicycle)
`q_root_size` a vector describing the sum of the q's associated to nodes in the tree rooted at each node
`holonomy_to_root` a vector describing the holonomy along the path from each node to its root in the MTSF
`importance_weight` the weight associated to the sampled forest
"""
mutable struct SmoothingMTSF
	root_of::Vector{Node}
	q_root_size::Vector{Float64}
	holonomy_to_root::Vector{ComplexF64}
	importance_weight::Float64
end

"""
    SmoothingMTSF(n::UInt64)

Constructs a `SmoothingMTSF` associated to a graph of size `n`.
"""
function SmoothingMTSF(n::UInt64)
	root_of = Vector{Node}(undef,n)
	q_root_size = Vector{Float64}(undef,n)
	holonomy_to_root = Vector{ComplexF64}(undef,n)
	importance_weight = Float64(1)
	return SmoothingMTSF(root_of,q_root_size,holonomy_to_root,importance_weight)
end


"""
    init_MTSF!(phi::SmoothingMTSF,n::UInt64)

(Re)-Initializes the `SmoothingMTSF` `phi`.
"""
function init_MTSF!(phi::SmoothingMTSF,n::UInt64)
	phi.root_of .= Node(0)
	phi.q_root_size .= Float64(0)
	phi.holonomy_to_root .= ComplexF64(1)
	phi.importance_weight = Float64(1)
	
	return
end

"""
    Counter

A counter used in the MTSF sampling algorithm.
"""
mutable struct Counter
	id::UInt64
	value::UInt64
end

"""
    Buffer data used in the MTSF sampling algorithm.

`next` a vector describing the next node in the path sampled during the process
`local_holonomy` a vector keeping track of the holonomy accumulated along the path to each node during the sampling process
`state` a vector describing whether a node belongs to a unicycle or a (rooted) tree
`cnt` an array of `Counter`s associated to each node
`cnt_max` an array recording the maximum value a `Counter` (as identified by his id)
"""
mutable struct SamplingBuffer
	next::Vector{UInt64}
	local_holonomy::Vector{Float64}
	state::Vector{UInt8}
	cnt::Vector{Counter}
	cnt_max::Vector{UInt64}
end

"""
    SamplingBuffer(n::UInt64)

Constructs a `SamplingBuffer` associated to a graph of size `n`.
"""
function SamplingBuffer(n::UInt64)
	next = Vector{UInt64}(undef,n)
	local_holonomy = Vector{Float64}(undef,n)
	state = Vector{UInt8}(undef,n)
	cnt = Counter[Counter(UInt64(0),UInt64(0)) for i in 1:n] # could be faster, but convenient
	cnt_max = UInt64[]

	return SamplingBuffer(next,local_holonomy,state,cnt,cnt_max)
end

"""
    init_buffer!(buf::SamplingBuffer,n::UInt64)

(Re)-Initializes the `SamplingBuffer` `buf`.
"""
function init_buffer!(buf::SamplingBuffer,n::UInt64)
	buf.next .= Node(0)
	buf.local_holonomy .= ComplexF64(1)
	buf.state .= UInt8(0)
	
	@inbounds for i in 1:n
		buf.cnt[i].id = UInt64(0)
		buf.cnt[i].value = UInt64(0)
	end

	buf.cnt_max = UInt64[]
	
	return
end

"""
    NeighborInfo

Contains information relative to the neighborhood of a node.

`list` the list of neighboring `Node`s
`weight` the weights of edges to said neighbors
`offset` the angular offsets of the edges to said neighbors
"""
struct NeighborInfo
	list::Vector{Node}
	weight::Vector{Float64}
	offset::Vector{Float64}
end

""" 
    NeighborInfo(v::Node,deg::Vector{UInt64})

Allocates the NeighborInfo for `Node` `v`, but does *not* fill in this information.
"""
function NeighborInfo(v::Node,deg::Vector{UInt64})
	list = Vector{Node}(undef,deg[v])
	weight = Vector{Float64}(undef,deg[v])
	offset = Vector{Float64}(undef,deg[v])

	return NeighborInfo(list,weight,offset)
end

"""
    ConnectionGraph

Contains the data describing a graph endowed with a unitary connection.

`g` the underlying graph
`C` the connection matrix
`n` the number of nodes in the graph
`degree` the vector of weighted degrees
`L` the connection Laplacian

`neighbors` is used in the MTSF sampling algorithm and contains the NeighborInfo for all nodes

Note that `L` = diagm(`degree`) - conj.(`C`).
"""
struct ConnectionGraph
	g::SimpleGraph{UInt64}
	C::Hermitian{ComplexF64, SparseMatrixCSC{ComplexF64, Int64}}

	n::UInt64
	degree::Vector{UInt64}
	L::Hermitian{ComplexF64, SparseMatrixCSC{ComplexF64, Int64}}

	neighbors::Vector{NeighborInfo}
end

"""
    WeightedConnectionGraph

Contains the data describing a weighted graph endowed with a unitary connection.

`g` the underlying graph
`C` the connection matrix
`n` the number of nodes in the graph
`degree` the vector of weighted degrees
`c_degree` the vector of combinatorial degrees
`L` the connection Laplacian

`neighbors` is used in the MTSF sampling algorithm and contains the NeighborInfo for all nodes

Note that `L` = diagm(`degree`) - conj.(`C`).
"""
struct WeightedConnectionGraph
	g::SimpleGraph{UInt64}
	C::Hermitian{ComplexF64, SparseMatrixCSC{ComplexF64, Int64}}

	n::UInt64
	degree::Vector{Float64}
	c_degree::Vector{UInt64}
	L::Hermitian{ComplexF64, SparseMatrixCSC{ComplexF64, Int64}}

	neighbors::Vector{NeighborInfo}
end

"""
    ConnectionGraph(graph::SimpleGraph{UInt64},connection_matrix::SparseMatrixCSC{ComplexF64, Int64},weight_matrix::SparseMatrixCSC{Float64, Int64})

Constructs a `ConnectionGraph` or `WeightedConnectionGraph` based on the underlying `graph`, with specified sparse `connection_matrix` and `weight_matrix`.
"""
function ConnectionGraph(graph::SimpleGraph{UInt64},connection_matrix::SparseMatrixCSC{ComplexF64, Int64},weight_matrix::SparseMatrixCSC{Float64, Int64})
	g = graph
	n = UInt64(Graphs.nv(g))

	C = connection_matrix
	
	degree = vec(sum(weight_matrix,dims=1))
	c_degree = UInt64.(Graphs.degree(g))

	laplacian_matrix = Diagonal(degree) .- conj.(C .* weight_matrix)
	
	weighted = false
	first_edge = true
	weight = Float64(0)

	adj = adjacency_matrix(g)
	
	neighbors = [NeighborInfo(v,c_degree) for v in 1:n]

	local_idx = ones(Int64,n)

	for e in edges(g)
		i = e.src
		j = e.dst
		if first_edge
			weight = weight_matrix[i,j]
			first_edge = false
		end

		neighbors[i].list[local_idx[i]] = Node(j)
		neighbors[i].weight[local_idx[i]] = weight_matrix[i,j]
		neighbors[i].offset[local_idx[i]] = angle(connection_matrix[i,j])

		neighbors[j].list[local_idx[j]] = Node(i)
		neighbors[j].weight[local_idx[j]] = weight_matrix[j,i]
		neighbors[j].offset[local_idx[j]] = angle(connection_matrix[j,i])

		local_idx[i] += 1
		local_idx[j] += 1

		if !isapprox(weight,weight_matrix[i,j])
			weighted = true
		end
	end
		
	if weighted
		return WeightedConnectionGraph(g,Hermitian(C),n,degree,c_degree,Hermitian(laplacian_matrix),neighbors)
	end

	return ConnectionGraph(g,Hermitian(C),n,c_degree,Hermitian(laplacian_matrix),neighbors)
end

"""
    AliasBuffer

A buffer structure used for the alias method.

`table` the alias table
`prb` the acceptance probability tables
"""
struct AliasBuffer
	table::Vector{Vector{Int64}}
	prb::Vector{Vector{Float64}}
end

"""
    AliasBuffer(CG::ConnectionGraph)

Instantiates an AliasBuffer using the `WeightedConnectionGraph` `CG`.
"""
function AliasBuffer(CG::WeightedConnectionGraph)
	table = [Vector{Int64}(undef,CG.c_degree[i]) for i in 1:CG.n]
	prb = [Vector{Float64}(undef,CG.c_degree[i]) for i in 1:CG.n]

	@inbounds for u in 1:CG.n
		StatsBase.make_alias_table!(CG.neighbors[u].weight,sum(CG.neighbors[u].weight),prb[u],table[u])
	end

	return AliasBuffer(table,prb)
end
