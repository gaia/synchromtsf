using SNAPDatasets, CoDeBetHe, Distributions
include("../src/header.jl")

function generate_graph(graph_id::Int64;synchro_a::Float64=Inf)
	n = UInt64(0)
	g_str = ""

	if graph_id == 1
		n = UInt64(10000)
		d = 3
		eps = 0.1
		g = epsilon_graph(n,eps,d)
		g_str = "epsilon"
		bl = 5
		ill = false
		alpha = 0
		alpha_c = 0
	elseif graph_id == 2
		n = UInt64(10000)
		c = 40.
		cin = 2*9. ; cout = 2*1.
		theta = ones(n)
		theta ./= mean(theta)
		C = [cin cout;
			cout cin]*2
		l = create_label_vector(n,2,[0.5,0.5])
		A = adjacency_matrix_DCSBM(C,c,l,theta)
		g = SimpleGraph{UInt64}(A)
		g_str = "sbm"
		bl = 2
		ill = true
		alpha = (cin - cout) / sqrt((cin + cout)/2)
		alpha_c = (2/sqrt(sum(theta .^ 2)/n))
	elseif graph_id == 3
		n = UInt64(10000)
		c = 40.
		cin = 2*9. ; cout = 2*1.
		mixt = MixtureModel(Normal,[(50,20),(500,100),(10000,100),],[0.69,0.3,0.01])
		theta = rand(mixt,n)
		theta ./= mean(theta)
		C = [cin cout;
			cout cin]*2
		l = create_label_vector(n,2,[0.5,0.5])
		A = adjacency_matrix_DCSBM(C,c,l,theta)
		g = SimpleGraph{UInt64}(A)
		g_str = "dc-sbm-1"
		bl = 2
		ill = true
		alpha = (cin - cout) / sqrt((cin + cout)/2)
		alpha_c = (2/sqrt(sum(theta .^ 2)/n))
	elseif graph_id == 4
		n = UInt64(10000)
		c = 500.
		cin = 2*120. ; cout = 2*5.
		mixt = MixtureModel(Normal,[(50,20),(1000,50),(5000,100),(10000,100),],[0.45,0.1,0.44,0.01])
		theta = rand(mixt,n)
		theta ./= mean(theta)
		C = [cin cout;
			cout cin]*2
		l = create_label_vector(n,2,[0.5,0.5])
		A = adjacency_matrix_DCSBM(C,c,l,theta)
		g = SimpleGraph{UInt64}(A)
		g_str = "dc-sbm-2"
		bl = 2
		ill = true
		alpha = (cin - cout) / sqrt((cin + cout)/2)
		alpha_c = (2/sqrt(sum(theta .^ 2)/n))
	elseif graph_id == 5
		tmp_g = loadsnap(:as_caida)
		n = UInt64(Graphs.nv(tmp_g))
		g = Graphs.SimpleGraph{UInt64}(tmp_g)
		g_str = "as_caida"
		bl = 5
		ill = true 
		alpha = 0
		alpha_c = 0
	elseif graph_id == 6
		n = UInt64(300)
		av_deg = 40
		p = av_deg/n
		g = Graphs.erdos_renyi(n,p)
		while !is_connected(g)
			g = Graphs.erdos_renyi(n,p)
		end
		g_str = "erdos_renyi"
		bl = 5
		ill = true
		alpha = 0
		alpha_c = 0
	else
		@info "invalid graph id"
		return
	end

	# in case the graph has nodes with degree 0, remove them
	while !is_connected(g)
		C = connected_components(g)
		if length(C[length(C)]) == 1
			rem_vertex!(g,C[length(C)][1])
		end
	end

	Y = ComplexF64.(adjacency_matrix(g))
	x = unit_part.(randn(ComplexF64,nv(g)))
	if synchro_a == Inf
		multiplicative_perturbation!(Y,x)
	else
		multiplicative_perturbation!(Y,x,a=synchro_a)
	end
	weight_matrix = abs.(Y)
	connection_matrix = unit_part.(Y)

	CG = ConnectionGraph(g,sparse(connection_matrix),sparse(weight_matrix))

	@info "$(g_str)"
	
	return CG,x,g_str,bl,ill,alpha,alpha_c
end
