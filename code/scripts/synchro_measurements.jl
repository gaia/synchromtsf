using BenchmarkTools, JLD, Plots, IterativeSolvers, IncompleteLU, Preconditioners
import KrylovKit.eigsolve as eigsolve

include("../src/header.jl")
include("graphs.jl")

BenchmarkTools.DEFAULT_PARAMETERS.seconds = Inf
BenchmarkTools.DEFAULT_PARAMETERS.evals = 1
LinearAlgebra.BLAS.set_num_threads(1)

# Power Methods

function pm_cg(M::SparseMatrixCSC{ComplexF64, Int64},init::Vector{ComplexF64},m::Int64,k::Int64;drop_tol::Float64=0.1)
	return power_method(init,(x,y) -> cg!(y,M,x,maxiter=m);nb_steps=k)
end

function pm_dpcg(M::SparseMatrixCSC{ComplexF64, Int64},init::Vector{ComplexF64},m::Int64,k::Int64;drop_tol::Float64=0.1)
	p = DiagonalPreconditioner(M)
	return power_method(init,(x,y) -> cg!(y,M,x,Pl=p,maxiter=m);nb_steps=k)
end

function pm_rb_mtsf(CG::ConnectionGraph,q_vector::Vector{Float64},init::Vector{ComplexF64},m::Int64,k::Int64)
	phi = SmoothingMTSF(CG.n)
	buf = SamplingBuffer(CG.n)
	
	tmp_f = Vector{ComplexF64}(undef,CG.n)

	return power_method(init,(x,y) -> _rb_smooth_with_MTSF!(CG,q_vector,x,m,phi,buf,y,tmp_f),nb_steps=k)
end

function pm_rb_cv_mtsf(CG::ConnectionGraph,q_vector::Vector{Float64},init::Vector{ComplexF64},m::Int64,k::Int64)
	phi = SmoothingMTSF(CG.n)
	buf = SamplingBuffer(CG.n)
	
	tmp_f = Vector{ComplexF64}(undef,CG.n)

	return power_method(init,(x,y) -> _rb_cv_smooth_with_MTSF!(CG,q_vector,x,m,phi,buf,y,tmp_f),nb_steps=k)
end

# Actual measurements

function synchro_optimal_q(CG::ConnectionGraph)
	eigenvalues = eigsolve(CG.L,2,:SR,maxiter=10000)[1]
	return sqrt(eigenvalues[1]*eigenvalues[2])
end

m_range = [3,10]
k_range = Int64.(ceil.(10 .^ range(log10(1),log10(100),10)))

function run_measurements_synchro(graph_id::Int64, save_directory::String, a::Float64; n_t::Int64=100,n_e::Int64=20,n_g=10)
	m_size = length(m_range)

	k_size = length(k_range)

	pm_cg_runtime = zeros(Float64,m_size,k_size)
	pm_cg_approximation_error = zeros(Float64,m_size,k_size,n_g*n_e)
	pm_cg_reconstruction_error = zeros(Float64,m_size,k_size,n_g*n_e)

	pm_dpcg_runtime = zeros(Float64,m_size,k_size)
	pm_dpcg_approximation_error = zeros(Float64,m_size,k_size,n_g*n_e)
	pm_dpcg_reconstruction_error = zeros(Float64,m_size,k_size,n_g*n_e)

	pm_rb_mtsf_runtime = zeros(Float64,m_size,k_size)
	pm_rb_mtsf_approximation_error = zeros(Float64,m_size,k_size,n_g*n_e)
	pm_rb_mtsf_reconstruction_error = zeros(Float64,m_size,k_size,n_g*n_e)

	pm_rb_cv_mtsf_runtime = zeros(Float64,m_size,k_size)
	pm_rb_cv_mtsf_approximation_error = zeros(Float64,m_size,k_size,n_g*n_e)
	pm_rb_cv_mtsf_reconstruction_error = zeros(Float64,m_size,k_size,n_g*n_e)

	alpha_list = zeros(Float64,n_g)
	alpha_c_list = zeros(Float64,n_g)
	mean_degree_list = zeros(Float64,n_g)
	max_degree_list = zeros(Float64,n_g)
	min_degree_list = zeros(Float64,n_g)
	n_list = zeros(Float64,n_g)

	double_list_idx = Int64(1)
	
	g_str = ""

	ust_mean_error = Float64(0)
	mst_mean_error = Float64(0)
	lanczos_mean_runtime = Float64(0)
	eigenvector_error = Float64(0)
	random_mean_error = Float64(0)

	for g_idx in 1:n_g
		@info "graph generation #$(g_idx)"
		CG,x,g_str,bandlimit,ill_cond,alpha,alpha_c = generate_graph(graph_id,synchro_a=a)
		@info "graph generated"

		alpha_list[g_idx] = alpha
		alpha_c_list[g_idx] = alpha_c
		mean_degree_list[g_idx] = mean(degree(CG.g))
		max_degree_list[g_idx] = maximum(degree(CG.g))
		min_degree_list[g_idx] = minimum(degree(CG.g))
		n_list[g_idx] = nv(CG.g)

		q = 0.01 * mean(degree(CG.g))
		q_vector = q * ones(CG.n)
	
		eigenvector = eigsolve(CG.L,1,:SR,maxiter=10000,issymmetric=false,ishermitian=true)[2][1]
		eigenvector_error += unitary_aligned_l2_loss(unit_part.(eigenvector),x)/(n_g * CG.n)

		M = inv(Diagonal(q_vector)) * (CG.L + Diagonal(q_vector))
	
		for e_idx in 1:n_e
			init = unit_part.(randn(ComplexF64,CG.n))

			@info "measurements $(e_idx)/$(n_e)"
			ust_mean_error += unitary_aligned_l2_loss(ust_synchronization(CG),x)/(n_g*n_e*CG.n)
			mst_mean_error += unitary_aligned_l2_loss(mst_synchronization(CG),x)/(n_g*n_e*CG.n)
			random_mean_error += unitary_aligned_l2_loss(init,x)/(n_g*n_e*CG.n)

			for k_idx in 1:k_size
				k = k_range[k_idx]
				@info "k = $(k)"
		
				for m_idx in 1:m_size
					m = m_range[m_idx]
					@info "m = $(m)"

					if e_idx == 1	
						# note: runtime independent of the input init signal
						# we only average over the random graphs except for Lanczos
	
						b = @benchmarkable eigsolve($(CG.L),$1,$:SR,maxiter=$(10000),issymmetric=$false,ishermitian=$true) samples=n_t
						lanczos_mean_runtime += mean(run(b).times)/(n_g*k_size*m_size)
						b = @benchmarkable pm_cg($M,$init,$m,$k) samples=n_t
						pm_cg_runtime[m_idx,k_idx] += mean(run(b).times)/n_g
						b = @benchmarkable pm_dpcg($M,$init,$m,$k) samples=n_t
						pm_dpcg_runtime[m_idx,k_idx] += mean(run(b).times)/n_g
						b = @benchmarkable pm_rb_mtsf($CG,$q_vector,$init,$m,$k) samples=n_t
						pm_rb_mtsf_runtime[m_idx,k_idx] += mean(run(b).times)/n_g
						b = @benchmarkable pm_rb_cv_mtsf($CG,$q_vector,$init,$m,$k) samples=n_t
						pm_rb_cv_mtsf_runtime[m_idx,k_idx] += mean(run(b).times)/n_g
					end
	
					pm_cg_reconstruction_error[m_idx,k_idx,double_list_idx] = unitary_aligned_l2_loss(unit_part.(pm_cg(M,init,m,k)),x)/CG.n
					pm_dpcg_reconstruction_error[m_idx,k_idx,double_list_idx] = unitary_aligned_l2_loss(unit_part.(pm_dpcg(M,init,m,k)),x)/CG.n
					pm_rb_mtsf_reconstruction_error[m_idx,k_idx,double_list_idx] = unitary_aligned_l2_loss(unit_part.(pm_rb_mtsf(CG,q_vector,init,m,k)),x)/CG.n
					pm_rb_cv_mtsf_reconstruction_error[m_idx,k_idx,double_list_idx] = unitary_aligned_l2_loss(unit_part.(pm_rb_cv_mtsf(CG,q_vector,init,m,k)),x)/CG.n

					end
				end
			double_list_idx += 1
		end
	end
	
	# aggregate the measurements and save in a jld file
	pm_res_cg = [pm_cg_runtime, pm_cg_approximation_error, pm_cg_reconstruction_error]
	pm_res_dpcg = [pm_dpcg_runtime, pm_dpcg_approximation_error, pm_dpcg_reconstruction_error]
	pm_res_rb_mtsf = [pm_rb_mtsf_runtime, pm_rb_mtsf_approximation_error, pm_rb_mtsf_reconstruction_error]
	pm_res_rb_cv_mtsf = [pm_rb_cv_mtsf_runtime, pm_rb_cv_mtsf_approximation_error, pm_rb_cv_mtsf_reconstruction_error]

	lanczos = [lanczos_mean_runtime,eigenvector_error]
	ust = [ust_mean_error]
	mst = [mst_mean_error]
	random = [random_mean_error]

	res = [pm_res_cg,pm_res_rb_mtsf,pm_res_rb_cv_mtsf,lanczos,ust,pm_res_dpcg,mst,random,alpha_list,alpha_c_list,g_str,mean_degree_list,max_degree_list,min_degree_list,n_list]

	if save_directory != ""
		@info "saving in $(save_directory)"
		save("$(save_directory)/synchro_output.jld","res",res)
	end

	return res
end

function plot_runtimes_synchro(measurements_location::String,save_location::String)
	pm_res_cg, pm_res_rb_mtsf, pm_res_rb_cv_mtsf, lanczos, ust, pm_res_dpcg, mst, random, alpha_list, alpha_c_list, g_str, mean_degree_list, max_degree_list, min_degree_list, n_list = load(measurements_location)["res"]
	
	dump_string = """
	graph: $(g_str)
	average n = $(mean(n_list))
	max n = $(maximum(n_list))
	min n = $(minimum(n_list))
	average mean degree = $(mean(mean_degree_list))
	average max degree = $(mean(max_degree_list))
	max degree = $(maximum(max_degree_list))
	average min degree = $(mean(min_degree_list))
	min degree = $(minimum(min_degree_list))
	average alpha = $(mean(alpha_list))
	average alpha_c = $(mean(alpha_c_list))
	"""

	runtime_normalization = 1e9

	ust_mean_error = ust[1]
	mst_mean_error = mst[1]
	init_mean_error = random[1]
	lanczos_mean_error = lanczos[2]
	lanczos_mean_runtime = lanczos[1]/(runtime_normalization) # account for all variations of parameters

	rb_mtsf_runtime = zeros(Float64,length(m_range),length(k_range))
	rb_mtsf_mean_error = zeros(Float64,length(m_range),length(k_range))
	rb_mtsf_var_error = zeros(Float64,length(m_range),length(k_range))

	rb_cv_mtsf_runtime = zeros(Float64,length(m_range),length(k_range))
	rb_cv_mtsf_mean_error = zeros(Float64,length(m_range),length(k_range))
	rb_cv_mtsf_var_error = zeros(Float64,length(m_range),length(k_range))

	cg_runtime = zeros(Float64,length(m_range),length(k_range))
	cg_mean_error = zeros(Float64,length(m_range),length(k_range))
	cg_var_error = zeros(Float64,length(m_range),length(k_range))

	dpcg_runtime = zeros(Float64,length(m_range),length(k_range))
	dpcg_mean_error = zeros(Float64,length(m_range),length(k_range))
	dpcg_var_error = zeros(Float64,length(m_range),length(k_range))

	for m_idx in 1:length(m_range)
		for k_idx in 1:length(k_range)
			rb_mtsf_runtime[m_idx,k_idx] = pm_res_rb_mtsf[1][m_idx,k_idx]/runtime_normalization
			rb_mtsf_mean_error[m_idx,k_idx] = mean(pm_res_rb_mtsf[3][m_idx,k_idx,:])
			rb_mtsf_var_error[m_idx,k_idx] = sqrt(var(pm_res_rb_mtsf[3][m_idx,k_idx,:]))

			rb_cv_mtsf_runtime[m_idx,k_idx] = pm_res_rb_cv_mtsf[1][m_idx,k_idx]/runtime_normalization
			rb_cv_mtsf_mean_error[m_idx,k_idx] = mean(pm_res_rb_cv_mtsf[3][m_idx,k_idx,:])
			rb_cv_mtsf_var_error[m_idx,k_idx] = sqrt(var(pm_res_rb_cv_mtsf[3][m_idx,k_idx,:]))

			cg_runtime[m_idx,k_idx] = pm_res_cg[1][m_idx,k_idx]/runtime_normalization
			cg_mean_error[m_idx,k_idx] = mean(pm_res_cg[3][m_idx,k_idx,:])
			cg_var_error[m_idx,k_idx] = sqrt(var(pm_res_cg[3][m_idx,k_idx,:]))

			dpcg_runtime[m_idx,k_idx] = pm_res_dpcg[1][m_idx,k_idx]/runtime_normalization
			dpcg_mean_error[m_idx,k_idx] = mean(pm_res_dpcg[3][m_idx,k_idx,:])
			dpcg_var_error[m_idx,k_idx] = sqrt(var(pm_res_dpcg[3][m_idx,k_idx,:]))
		end
	end

	kticks = 10. .^ collect(-3:1) # for each fixed k

	pgfplotsx()

	for m_idx in 1:length(m_range)
		m = m_range[m_idx]
	
		plot(rb_mtsf_runtime[m_idx,:],rb_mtsf_mean_error[m_idx,:],marker=4,label="MTSF",legend=:topright,markerstrokecolor=:auto,linewidth=2,xaxis=:log,yaxis=:log)
		plot!(rb_cv_mtsf_runtime[m_idx,:],rb_cv_mtsf_mean_error[m_idx,:],marker=4,label="MTSF+GS",markerstrokecolor=:auto,linewidth=2)
		plot!(cg_runtime[m_idx,:],cg_mean_error[m_idx,:],marker=4,label="CG",xlabel="Time in s",ylabel="Synchronization error",xaxis=:log,yaxis=:log,ticks=true,markerstrokecolor=:auto,linewidth=2)
		plot!(dpcg_runtime[m_idx,:],dpcg_mean_error[m_idx,:],marker=4,label="D^{-1}+CG",xlabel="Time in s",ylabel="Synchronization error",xaxis=:log,yaxis=:log,ticks=true,markerstrokecolor=:auto,linewidth=2)
		vline!([lanczos_mean_runtime],line=:dot,label="",linewidth=2)
		hline!([lanczos_mean_error],line=:dot,label="Lanczos",linewidth=2)
		hline!([ust_mean_error],line=:dot,label="UST",linewidth=2)
		hline!([mst_mean_error],line=:dot,label="MST",linewidth=2)
		hline!([init_mean_error],line=:dot,label="INIT",linewidth=2)
		xticks!(kticks)
		savefig("$(save_location)/synchro_$(g_str)_m_$(m).tikz")
	end

	gr()

	for m_idx in 1:length(m_range)
		m = m_range[m_idx]
	
		plot(rb_mtsf_runtime[m_idx,:],rb_mtsf_mean_error[m_idx,:],marker=4,label="MTSF",legend=:topright,markerstrokecolor=:auto,linewidth=2,xaxis=:log,yaxis=:log)
		plot!(rb_cv_mtsf_runtime[m_idx,:],rb_cv_mtsf_mean_error[m_idx,:],marker=4,label="MTSF+GS",markerstrokecolor=:auto,linewidth=2)
		plot!(cg_runtime[m_idx,:],cg_mean_error[m_idx,:],marker=4,label="CG",xlabel="Time in s",ylabel="Synchronization error",xaxis=:log,yaxis=:log,ticks=true,markerstrokecolor=:auto,linewidth=2)
		plot!(dpcg_runtime[m_idx,:],dpcg_mean_error[m_idx,:],marker=4,label="D^{-1}+CG",xlabel="Time in s",ylabel="Synchronization error",xaxis=:log,yaxis=:log,ticks=true,markerstrokecolor=:auto,linewidth=2)
		vline!([lanczos_mean_runtime],line=:dot,label="",linewidth=2)
		hline!([lanczos_mean_error],line=:dot,label="Lanczos",linewidth=2)
		hline!([ust_mean_error],line=:dot,label="UST",linewidth=2)
		hline!([mst_mean_error],line=:dot,label="MST",linewidth=2)
		hline!([init_mean_error],line=:dot,label="INIT",linewidth=2)
		xticks!(kticks)
		savefig("$(save_location)/synchro_$(g_str)_m_$(m).pdf")
	end

	write(joinpath(save_location,"info.txt"),dump_string)

	return
end

_,_,g_str,_,_,_,_ = generate_graph(2)
run_measurements_synchro(2,"../data/runtime/synchro/$(g_str)",Inf)
plot_runtimes_synchro("../data/runtime/synchro/$(g_str)/synchro_output.jld","../plots/simods/synchro/$(g_str)")

_,_,g_str,_,_,_,_ = generate_graph(3)
run_measurements_synchro(3,"../data/runtime/synchro/$(g_str)",Inf)
plot_runtimes_synchro("../data/runtime/synchro/$(g_str)/synchro_output.jld","../plots/simods/synchro/$(g_str)")

_,_,g_str,_,_,_,_ = generate_graph(3,synchro_a=pi/10)
run_measurements_synchro(3,"../data/runtime/synchro/$(g_str)/noisy",pi/10)
plot_runtimes_synchro("../data/runtime/synchro/$(g_str)/noisy/synchro_output.jld","../plots/simods/synchro/$(g_str)/noisy")

_,_,g_str,_,_,_,_ = generate_graph(4)
run_measurements_synchro(4,"../data/runtime/synchro/$(g_str)",Inf)
plot_runtimes_synchro("../data/runtime/synchro/$(g_str)/synchro_output.jld","../plots/simods/synchro/$(g_str)")
