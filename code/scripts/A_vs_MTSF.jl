using BenchmarkTools, JLD, Plots, IterativeSolvers, IncompleteLU, Preconditioners
import KrylovKit.eigsolve as eigsolve

include("../src/header.jl")
include("graphs.jl")

BenchmarkTools.DEFAULT_PARAMETERS.seconds = Inf
BenchmarkTools.DEFAULT_PARAMETERS.evals = 1
LinearAlgebra.BLAS.set_num_threads(1)

# Power Methods

function pm_adj(A::SparseMatrixCSC{ComplexF64,Int64},init::Vector{ComplexF64},k::Int64)
	input = copy(init)
	output = Vector{ComplexF64}(undef,length(input))

	i = Int64(1)
	while i <= k
		output = A*input
		output ./= norm(output)

		i += 1
		input = output
	end
	return output
end

function pm_dpcg(M::SparseMatrixCSC{ComplexF64, Int64},init::Vector{ComplexF64},m::Int64,k::Int64;drop_tol::Float64=0.1)
	p = DiagonalPreconditioner(M)
	return power_method(init,(x,y) -> cg!(y,M,x,Pl=p,maxiter=m);nb_steps=k)
end

function pm_rb_cv_mtsf(CG::ConnectionGraph,q_vector::Vector{Float64},init::Vector{ComplexF64},m::Int64,k::Int64)
	phi = SmoothingMTSF(CG.n)
	buf = SamplingBuffer(CG.n)
	
	tmp_f = Vector{ComplexF64}(undef,CG.n)

	return power_method(init,(x,y) -> _rb_cv_smooth_with_MTSF!(CG,q_vector,x,m,phi,buf,y,tmp_f),nb_steps=k)
end

# Actual measurements

m_range = [5] 
k_range = Int64.(ceil.(10 .^ range(log10(1),log10(300),10)))
k_range[10] = Int64(300)

function run_measurements_synchro(graph_id::Int64, save_directory::String, a::Float64; n_t::Int64=100,n_e::Int64=10,n_g=5)
	m_size = length(m_range)

	k_size = length(k_range)

	pm_adj_runtime = zeros(Float64,m_size,k_size)
	pm_adj_approximation_error = zeros(Float64,m_size,k_size,n_g*n_e)
	pm_adj_reconstruction_error = zeros(Float64,m_size,k_size,n_g*n_e)

	pm_dpcg_runtime = zeros(Float64,m_size,k_size)
	pm_dpcg_approximation_error = zeros(Float64,m_size,k_size,n_g*n_e)
	pm_dpcg_reconstruction_error = zeros(Float64,m_size,k_size,n_g*n_e)

	pm_rb_cv_mtsf_runtime = zeros(Float64,m_size,k_size)
	pm_rb_cv_mtsf_approximation_error = zeros(Float64,m_size,k_size,n_g*n_e)
	pm_rb_cv_mtsf_reconstruction_error = zeros(Float64,m_size,k_size,n_g*n_e)

	double_list_idx = Int64(1)

	eigenvector_error = Float64(0)
	eig_A_error = Float64(0)
	
	g_str = ""

	for g_idx in 1:n_g
		@info "graph generation #$(g_idx)"
		CG,x,g_str,bandlimit,ill_cond,alpha,alpha_c = generate_graph(graph_id,synchro_a=a)
		@info "graph generated"
		@info "$(g_str)"

		A = sparse(Diagonal(CG.degree) - CG.L)

		q = 0.0001 * mean(degree(CG.g))
		q_vector = q * ones(CG.n)
	
		eigenvector = eigsolve(CG.L,1,:SR,maxiter=10000,issymmetric=false,ishermitian=true)[2][1]
		eig_A = eigsolve(A,1,:LR,maxiter=10000)[2][1]

		eigenvector_error = unitary_aligned_l2_loss(unit_part.(eigenvector),x)/CG.n
		eig_A_error = unitary_aligned_l2_loss(unit_part.(eig_A),x)/CG.n

		M = inv(Diagonal(q_vector)) * (CG.L + Diagonal(q_vector))
	
		for e_idx in 1:n_e
			init = unit_part.(randn(ComplexF64,CG.n))

			@info "measurements $(e_idx)/$(n_e)"

			for k_idx in 1:k_size
				k = k_range[k_idx]
				@info "k = $(k)"
		
				for m_idx in 1:m_size
					m = m_range[m_idx]
					@info "m = $(m)"

					if e_idx == 1	
						# note: runtime independent of the input init signal
						# we only average over the random graphs except for Lanczos
	
						b = @benchmarkable pm_adj($A,$init,$k) samples=n_t
						pm_adj_runtime[m_idx,k_idx] += mean(run(b).times)/n_g
						b = @benchmarkable pm_dpcg($M,$init,$m,$k) samples=n_t
						pm_dpcg_runtime[m_idx,k_idx] += mean(run(b).times)/n_g
						b = @benchmarkable pm_rb_cv_mtsf($CG,$q_vector,$init,$m,$k) samples=n_t
						pm_rb_cv_mtsf_runtime[m_idx,k_idx] += mean(run(b).times)/n_g
					end
	
					pm_adj_reconstruction_error[m_idx,k_idx,double_list_idx] = unitary_aligned_l2_loss(unit_part.(pm_adj(A,init,k)),x)/CG.n
					pm_dpcg_reconstruction_error[m_idx,k_idx,double_list_idx] = unitary_aligned_l2_loss(unit_part.(pm_dpcg(M,init,m,k)),x)/CG.n
					pm_rb_cv_mtsf_reconstruction_error[m_idx,k_idx,double_list_idx] = unitary_aligned_l2_loss(unit_part.(pm_rb_cv_mtsf(CG,q_vector,init,m,k)),x)/CG.n

					end
				end
			double_list_idx += 1
		end
	end
	
	# aggregate the measurements and save in a jld file
	pm_res_adj = [pm_adj_runtime, pm_adj_approximation_error, pm_adj_reconstruction_error]
	pm_res_dpcg = [pm_dpcg_runtime, pm_dpcg_approximation_error, pm_dpcg_reconstruction_error]
	pm_res_rb_cv_mtsf = [pm_rb_cv_mtsf_runtime, pm_rb_cv_mtsf_approximation_error, pm_rb_cv_mtsf_reconstruction_error]

	res = [pm_res_adj,pm_res_rb_cv_mtsf,eigenvector_error,eig_A_error,pm_res_dpcg,g_str]

	if save_directory != ""
		@info "saving in $(save_directory)"
		save("$(save_directory)/A_vs_MTSF.jld","res",res)
	end

	return res
end

function plot_runtimes_synchro(measurements_location::String,save_location::String)
	pm_res_adj,pm_res_rb_cv_mtsf,eigenvector_error,eig_A_error,pm_res_dpcg,g_str = load(measurements_location)["res"]
	
	runtime_normalization = 1e9

	rb_cv_mtsf_runtime = zeros(Float64,length(m_range),length(k_range))
	rb_cv_mtsf_mean_error = zeros(Float64,length(m_range),length(k_range))
	rb_cv_mtsf_var_error = zeros(Float64,length(m_range),length(k_range))

	adj_runtime = zeros(Float64,length(m_range),length(k_range))
	adj_mean_error = zeros(Float64,length(m_range),length(k_range))
	adj_var_error = zeros(Float64,length(m_range),length(k_range))

	dpcg_runtime = zeros(Float64,length(m_range),length(k_range))
	dpcg_mean_error = zeros(Float64,length(m_range),length(k_range))
	dpcg_var_error = zeros(Float64,length(m_range),length(k_range))

	for m_idx in 1:length(m_range)
		for k_idx in 1:length(k_range)
			rb_cv_mtsf_runtime[m_idx,k_idx] = pm_res_rb_cv_mtsf[1][m_idx,k_idx]/runtime_normalization
			rb_cv_mtsf_mean_error[m_idx,k_idx] = mean(pm_res_rb_cv_mtsf[3][m_idx,k_idx,:])
			rb_cv_mtsf_var_error[m_idx,k_idx] = sqrt(var(pm_res_rb_cv_mtsf[3][m_idx,k_idx,:]))

			adj_runtime[m_idx,k_idx] = pm_res_adj[1][m_idx,k_idx]/runtime_normalization
			adj_mean_error[m_idx,k_idx] = mean(pm_res_adj[3][m_idx,k_idx,:])
			adj_var_error[m_idx,k_idx] = sqrt(var(pm_res_adj[3][m_idx,k_idx,:]))

			dpcg_runtime[m_idx,k_idx] = pm_res_dpcg[1][m_idx,k_idx]/runtime_normalization
			dpcg_mean_error[m_idx,k_idx] = mean(pm_res_dpcg[3][m_idx,k_idx,:])
			dpcg_var_error[m_idx,k_idx] = sqrt(var(pm_res_dpcg[3][m_idx,k_idx,:]))
		end
	end

	kticks = 10. .^ collect(-3:1) # for each fixed k

	pgfplotsx()

	for m_idx in 1:length(m_range)
		m = m_range[m_idx]
	
		plot(adj_runtime[m_idx,:],adj_mean_error[m_idx,:],marker=4,label="PM A",legend=:topright,markerstrokecolor=:auto,linewidth=2,xaxis=:log,yaxis=:log)
		plot!(rb_cv_mtsf_runtime[m_idx,:],rb_cv_mtsf_mean_error[m_idx,:],marker=4,label="iPM MTSF+GS",markerstrokecolor=:auto,linewidth=2)
		plot!(dpcg_runtime[m_idx,:],dpcg_mean_error[m_idx,:],marker=4,label="iPM D^{-1}+CG",xlabel="Time in s",ylabel="Synchronization error",xaxis=:log,yaxis=:log,ticks=true,markerstrokecolor=:auto,linewidth=2)
		hline!([eigenvector_error],line=:dot,label="Eigenvector L",linewidth=2)
		hline!([eig_A_error],line=:dot,label="Eigenvector A",linewidth=2)
		xticks!(kticks)
		savefig("$(save_location)/synchro_$(g_str)_m_$(m).tikz")
	end

	gr()

	for m_idx in 1:length(m_range)
		m = m_range[m_idx]
	
		plot(adj_runtime[m_idx,:],adj_mean_error[m_idx,:],marker=4,label="PM A",legend=:topright,markerstrokecolor=:auto,linewidth=2,xaxis=:log,yaxis=:log)
		plot!(rb_cv_mtsf_runtime[m_idx,:],rb_cv_mtsf_mean_error[m_idx,:],marker=4,label="iPM MTSF+GS",markerstrokecolor=:auto,linewidth=2)
		plot!(dpcg_runtime[m_idx,:],dpcg_mean_error[m_idx,:],marker=4,label="iPM D^{-1}+CG",xlabel="Time in s",ylabel="Synchronization error",xaxis=:log,yaxis=:log,ticks=true,markerstrokecolor=:auto,linewidth=2)
		hline!([eigenvector_error],line=:dot,label="Eigenvector L",linewidth=2)
		hline!([eig_A_error],line=:dot,label="Eigenvector A",linewidth=2)
		xticks!(kticks)
		savefig("$(save_location)/synchro_$(g_str)_m_$(m).pdf")
	end

	return
end

#_,_,g_str,_,_,_,_ = generate_graph(6)
#run_measurements_synchro(6,"../data/runtime/synchro/$(g_str)",Inf,n_g=1,n_e=1)
#run_measurements_synchro(6,"../data/runtime/synchro/$(g_str)",pi/10)
#plot_runtimes_synchro("../data/runtime/synchro/$(g_str)/A_vs_MTSF.jld","../plots/A_vs_MTSF/$(g_str)")

_,_,g_str,_,_,_,_ = generate_graph(1)
#run_measurements_synchro(1,"../data/runtime/synchro/$(g_str)",Inf)
#run_measurements_synchro(1,"../data/runtime/synchro/$(g_str)",pi/100)
plot_runtimes_synchro("../data/runtime/synchro/low_inc_archive/$(g_str)/A_vs_MTSF.jld","../plots/A_vs_MTSF/$(g_str)")

#_,_,g_str,_,_,_,_ = generate_graph(2)
#run_measurements_synchro(2,"../data/runtime/synchro/$(g_str)",Inf)
#run_measurements_synchro(2,"../data/runtime/synchro/$(g_str)",pi/100)
#plot_runtimes_synchro("../data/runtime/synchro/$(g_str)/A_vs_MTSF.jld","../plots/A_vs_MTSF/$(g_str)")

#_,_,g_str,_,_,_,_ = generate_graph(3)
#run_measurements_synchro(3,"../data/runtime/synchro/$(g_str)",Inf)
#run_measurements_synchro(3,"../data/runtime/synchro/$(g_str)",pi/100)
#plot_runtimes_synchro("../data/runtime/synchro/$(g_str)/A_vs_MTSF.jld","../plots/A_vs_MTSF/$(g_str)")

#_,_,g_str,_,_,_,_ = generate_graph(4)
#run_measurements_synchro(4,"../data/runtime/synchro/$(g_str)",Inf)
#run_measurements_synchro(4,"../data/runtime/synchro/$(g_str)",pi/100)
#plot_runtimes_synchro("../data/runtime/synchro/$(g_str)/A_vs_MTSF.jld","../plots/A_vs_MTSF/$(g_str)")


