using BenchmarkTools, JLD, Plots, IterativeSolvers, IncompleteLU, Preconditioners
import KrylovKit.eigsolve as eigsolve

include("../src/header.jl")
include("graphs.jl")

BenchmarkTools.DEFAULT_PARAMETERS.seconds = Inf
BenchmarkTools.DEFAULT_PARAMETERS.evals = 1
LinearAlgebra.BLAS.set_num_threads(1)

function cholesky_solver(M::Hermitian{ComplexF64, SparseMatrixCSC{ComplexF64, Int64}},noisy_signal::Vector{ComplexF64})
	chol = cholesky(M)
	return chol\noisy_signal
end

function pcg(M::SparseMatrixCSC{ComplexF64, Int64},noisy_signal::Vector{ComplexF64},m::Int64;drop_tol::Float64=0.1)
	p = ilu(M,τ=drop_tol)
	return cg(M,noisy_signal,Pl=p,maxiter=m)
end

function dpcg(M::SparseMatrixCSC{ComplexF64, Int64},noisy_signal::Vector{ComplexF64},m::Int64;drop_tol::Float64=0.1)
	p = DiagonalPreconditioner(M)
	return cg(M,noisy_signal,Pl=p,maxiter=m)
end

function k_bandlimited_signal(CG::ConnectionGraph,k::Int64)
	base_signal = zeros(ComplexF64,CG.n)

	eigenvectors = eigsolve(CG.L,k,:SR,maxiter=10000)[2]
	d = Normal(0.0,.5)
	for i in 1:k
		base_signal .+= (rand(d) + im*rand(d)) * eigenvectors[i]
	end

	return base_signal
end

function noise_corruption(base_signal::Vector{ComplexF64},SNR::Float64)
	sd = sqrt(1/(SNR*length(base_signal)))
	d = Normal(0.0,sd/2)
	return base_signal .+ rand(d,length(base_signal)) .+ (im .* rand(d,length(base_signal)))
end

function smoothing_optimal_q(CG::ConnectionGraph,base_signal::Vector{ComplexF64},noisy_signal::Vector{ComplexF64})
	opt_q = Float64(1)
	opt_error = Float64(Inf)

	q = Float64(1)
	q_vector = q*ones(CG.n)

	q_range = 10 .^ range(log10(1e-2),log10(30),30)
	
	error = Float64(Inf)

	M = spzeros(ComplexF64,(CG.n,CG.n))
	
	for q in collect(q_range)
		q_vector .= q
		M .= inv(Diagonal(q_vector)) * (CG.L + Diagonal(q_vector))
		error = norm(M\noisy_signal - base_signal)
		if error < opt_error
			opt_error = error
			opt_q = q
		end
	end

	return opt_q
end

# n_e number of realisations of the signal over which the measurements are averaged
# n_t number of runtime measurements (per realisation of the signal)
function run_measurements(graph_id::Int64,save_directory::String; n_t::Int64=100,n_e::Int64=5,n_g::Int64=10,SNR::Float64=2.)

	# create some arrays
	@info "allocating storage"
	m_range = Int64.(ceil.(10 .^ range(log10(1),log10(100),10)))

	rb_mtsf_runtime = zeros(Float64,length(m_range))
	rb_mtsf_approximation_error_hist = zeros(Float64,length(m_range),n_g*n_e)
	rb_mtsf_reconstruction_error_hist = zeros(Float64,length(m_range),n_g*n_e)
	
	rb_cv_mtsf_runtime = zeros(Float64,length(m_range))
	rb_cv_mtsf_approximation_error_hist = zeros(Float64,length(m_range),n_g*n_e)
	rb_cv_mtsf_reconstruction_error_hist = zeros(Float64,length(m_range),n_g*n_e)

	cg_runtime = zeros(Float64,length(m_range))
	cg_approximation_error_hist = zeros(Float64,length(m_range),n_g*n_e)
	cg_reconstruction_error_hist = zeros(Float64,length(m_range),n_g*n_e)

	pcg_runtime = zeros(Float64,length(m_range))
	pcg_approximation_error_hist = zeros(Float64,length(m_range),n_g*n_e)
	pcg_reconstruction_error_hist = zeros(Float64,length(m_range),n_g*n_e)

	dpcg_runtime = zeros(Float64,length(m_range))
	dpcg_approximation_error_hist = zeros(Float64,length(m_range),n_g*n_e)
	dpcg_reconstruction_error_hist = zeros(Float64,length(m_range),n_g*n_e)

	chol_mean_runtime = Float64(0)
	chol_mean_error = Float64(0)
	noisy_mean_error = Float64(0)

	alpha_list = zeros(Float64,n_g)
	alpha_c_list = zeros(Float64,n_g)
	mean_degree_list = zeros(Float64,n_g)
	max_degree_list = zeros(Float64,n_g)
	min_degree_list = zeros(Float64,n_g)
	n_list = zeros(Float64,n_g)

	q_list = zeros(Float64,n_g*n_e)
	double_list_idx = Int64(1)

	ill_cond = true

	g_str = ""

	for g_idx in 1:n_g
		@info "graph generation #$(g_idx)"
		CG,x,g_str,bandlimit,ill_cond,alpha,alpha_c = generate_graph(graph_id)

		alpha_list[g_idx] = alpha
		alpha_c_list[g_idx] = alpha_c
		mean_degree_list[g_idx] = mean(degree(CG.g))
		max_degree_list[g_idx] = maximum(degree(CG.g))
		min_degree_list[g_idx] = minimum(degree(CG.g))
		n_list[g_idx] = nv(CG.g)

		exact_smoothing = zeros(ComplexF64,CG.n)

		@info "base_signal #$(g_idx) generation"
		base_signal = k_bandlimited_signal(CG,bandlimit)

		# actually run the measurements
		@info "running measurements"
		for e_idx in 1:n_e
			noisy_signal = noise_corruption(base_signal,SNR)
			@info "- noisy signal #$(e_idx)"
			q = smoothing_optimal_q(CG,base_signal,noisy_signal)
			q_vector = q * ones(CG.n)

			q_list[double_list_idx] = q

			M = inv(Diagonal(q_vector)) * (CG.L + Diagonal(q_vector))
			hM = Hermitian(M)
			exact_smoothing = cholesky_solver(hM,noisy_signal)
		
			@info "cholesky baseline"
			b = @benchmarkable cholesky_solver($hM,$noisy_signal) samples=n_t
			chol_mean_runtime += mean(run(b).times)/(n_e*n_g)
			@info "- done"
			chol_mean_error += norm(exact_smoothing - base_signal)/(n_g*n_e*CG.n)
	
			noisy_mean_error += norm(noisy_signal - base_signal)/(n_g*n_e*CG.n)
	
			for m_idx in 1:length(m_range)
				m = m_range[m_idx]
				@info "m = $(m)"
		
				@info "- runtime"
				b = @benchmarkable rb_smooth_with_MTSF($CG,$q_vector,$noisy_signal,$m) samples=n_t
				rb_mtsf_runtime[m_idx] += mean(run(b).times)/(n_g*n_e)
				b = @benchmarkable rb_cv_smooth_with_MTSF($CG,$q_vector,$noisy_signal,$m) samples=n_t
				rb_cv_mtsf_runtime[m_idx] += mean(run(b).times)/(n_g*n_e)
				b = @benchmarkable cg($M,$noisy_signal,maxiter=$m) samples=n_t
				cg_runtime[m_idx] += mean(run(b).times)/(n_g*n_e)
				b = @benchmarkable pcg($M,$noisy_signal,$m) samples=n_t
				if !ill_cond
					pcg_runtime[m_idx] += mean(run(b).times)/(n_g*n_e)
				end
				b = @benchmarkable dpcg($M,$noisy_signal,$m) samples=n_t
				dpcg_runtime[m_idx] += mean(run(b).times)/(n_g*n_e)
		
				@info "- errors"
	
				# approximation errors
				rb_mtsf_approximation_error_hist[m_idx,double_list_idx] = norm(rb_smooth_with_MTSF(CG,q_vector,noisy_signal,m) - exact_smoothing)/CG.n
				rb_cv_mtsf_approximation_error_hist[m_idx,double_list_idx] = norm(rb_smooth_with_MTSF(CG,q_vector,noisy_signal,m) - exact_smoothing)/CG.n
				cg_approximation_error_hist[m_idx,double_list_idx] = norm(cg(M,noisy_signal,maxiter=m) - exact_smoothing)/CG.n
				if !ill_cond
					pcg_approximation_error_hist[m_idx,double_list_idx] = norm(pcg(M,noisy_signal,m) - exact_smoothing)/CG.n
				end
				dpcg_approximation_error_hist[m_idx,double_list_idx] = norm(dpcg(M,noisy_signal,m) - exact_smoothing)/CG.n
	
				# reconstruction errors
				rb_mtsf_reconstruction_error_hist[m_idx,double_list_idx] = norm(rb_smooth_with_MTSF(CG,q_vector,noisy_signal,m) - base_signal)/CG.n
				rb_cv_mtsf_reconstruction_error_hist[m_idx,double_list_idx] = norm(rb_cv_smooth_with_MTSF(CG,q_vector,noisy_signal,m) - base_signal)/CG.n
				cg_reconstruction_error_hist[m_idx,double_list_idx] = norm(cg(M,noisy_signal,maxiter=m) - base_signal)/CG.n
				if !ill_cond
					pcg_reconstruction_error_hist[m_idx,double_list_idx] = norm(pcg(M,noisy_signal,m) - base_signal)/CG.n
				end
				dpcg_reconstruction_error_hist[m_idx,double_list_idx] = norm(dpcg(M,noisy_signal,m) - base_signal)/CG.n
			end

			double_list_idx += 1

		end
	end
	
	# aggregate the measurements and save in a jld file
	res_rb_mtsf = [rb_mtsf_runtime, rb_mtsf_approximation_error_hist, rb_mtsf_reconstruction_error_hist]
	res_rb_cv_mtsf = [rb_cv_mtsf_runtime, rb_cv_mtsf_approximation_error_hist, rb_cv_mtsf_reconstruction_error_hist]
	res_cg = [cg_runtime, cg_approximation_error_hist, cg_reconstruction_error_hist]
	res_pcg = [pcg_runtime, pcg_approximation_error_hist, pcg_reconstruction_error_hist]
	res_dpcg = [dpcg_runtime, dpcg_approximation_error_hist, dpcg_reconstruction_error_hist]

	chol = [chol_mean_runtime, chol_mean_error]

	res = [res_rb_mtsf,res_rb_cv_mtsf,res_cg,res_pcg,chol,noisy_mean_error,res_dpcg,q_list,alpha_list,alpha_c_list,g_str,ill_cond,mean_degree_list,max_degree_list,min_degree_list,n_list]

	@info "saving in $(save_directory)"
	save("$(save_directory)/$(g_str)/output.jld","res",res)

	return res
end

function plot_runtimes(measurements_location::String,save_location::String)
	res_rb_mtsf, res_rb_cv_mtsf, res_cg, res_pcg, chol, noisy_mean_error, res_dpcg, q_list, alpha_list, alpha_c_list, g_str, ill_cond, mean_degree_list, max_degree_list, min_degree_list, n_list = load(measurements_location)["res"]

	dump_string = """
	graph: $(g_str)
	average n = $(mean(n_list))
	max n = $(maximum(n_list))
	min n = $(minimum(n_list))
	average mean degree = $(mean(mean_degree_list))
	average max degree = $(mean(max_degree_list))
	max degree = $(maximum(max_degree_list))
	average min degree = $(mean(min_degree_list))
	min degree = $(minimum(min_degree_list))
	average q = $(mean(q_list))
	average alpha = $(mean(alpha_list))
	average alpha_c = $(mean(alpha_c_list))
	"""

	m_size = length(res_cg[2][:,1])

	rb_mtsf_runtime = zeros(Float64,m_size)
	rb_mtsf_mean_approx = zeros(Float64,m_size)
	rb_mtsf_var_approx = zeros(Float64,m_size)
	rb_mtsf_mean_rec = zeros(Float64,m_size)
	rb_mtsf_var_rec = zeros(Float64,m_size)

	rb_cv_mtsf_runtime = zeros(Float64,m_size)
	rb_cv_mtsf_mean_approx = zeros(Float64,m_size)
	rb_cv_mtsf_var_approx = zeros(Float64,m_size)
	rb_cv_mtsf_mean_rec = zeros(Float64,m_size)
	rb_cv_mtsf_var_rec = zeros(Float64,m_size)
	
	cg_runtime = zeros(Float64,m_size)
	cg_mean_approx = zeros(Float64,m_size)
	cg_var_approx = zeros(Float64,m_size)
	cg_mean_rec = zeros(Float64,m_size)
	cg_var_rec = zeros(Float64,m_size)

	pcg_runtime = zeros(Float64,m_size)
	pcg_mean_approx = zeros(Float64,m_size)
	pcg_var_approx = zeros(Float64,m_size)
	pcg_mean_rec = zeros(Float64,m_size)
	pcg_var_rec = zeros(Float64,m_size)

	dpcg_runtime = zeros(Float64,m_size)
	dpcg_mean_approx = zeros(Float64,m_size)
	dpcg_var_approx = zeros(Float64,m_size)
	dpcg_mean_rec = zeros(Float64,m_size)
	dpcg_var_rec = zeros(Float64,m_size)

	runtime_normalization = 1e9

	for m_idx in 1:m_size
		rb_mtsf_runtime[m_idx] = res_rb_mtsf[1][m_idx]/runtime_normalization
		rb_mtsf_mean_approx[m_idx] = mean(res_rb_mtsf[2][m_idx,:])
		rb_mtsf_var_approx[m_idx] = sqrt(var(res_rb_mtsf[2][m_idx,:]))

		rb_cv_mtsf_runtime[m_idx] = res_rb_cv_mtsf[1][m_idx]/runtime_normalization
		rb_cv_mtsf_mean_approx[m_idx] = mean(res_rb_cv_mtsf[2][m_idx,:])
		rb_cv_mtsf_var_approx[m_idx] = sqrt(var(res_rb_cv_mtsf[2][m_idx,:]))

		cg_runtime[m_idx] = res_cg[1][m_idx]/runtime_normalization
		cg_mean_approx[m_idx] = mean(res_cg[2][m_idx,:])
		cg_var_approx[m_idx] = sqrt(var(res_cg[2][m_idx,:]))

		pcg_runtime[m_idx] = res_pcg[1][m_idx]/runtime_normalization
		pcg_mean_approx[m_idx] = mean(res_pcg[2][m_idx,:])
		pcg_var_approx[m_idx] = sqrt(var(res_pcg[2][m_idx,:]))

		dpcg_runtime[m_idx] = res_dpcg[1][m_idx]/runtime_normalization
		dpcg_mean_approx[m_idx] = mean(res_dpcg[2][m_idx,:])
		dpcg_var_approx[m_idx] = sqrt(var(res_dpcg[2][m_idx,:]))



		rb_mtsf_mean_rec[m_idx] = mean(res_rb_mtsf[3][m_idx,:])
		rb_mtsf_var_rec[m_idx] = sqrt(var(res_rb_mtsf[3][m_idx,:]))

		rb_cv_mtsf_mean_rec[m_idx] = mean(res_rb_cv_mtsf[3][m_idx,:])
		rb_cv_mtsf_var_rec[m_idx] = sqrt(var(res_rb_cv_mtsf[3][m_idx,:]))

		cg_mean_rec[m_idx] = mean(res_cg[3][m_idx,:])
		cg_var_rec[m_idx] = sqrt(var(res_cg[3][m_idx,:]))

		pcg_mean_rec[m_idx] = mean(res_pcg[3][m_idx,:])
		pcg_var_rec[m_idx] = sqrt(var(res_pcg[3][m_idx,:]))

		dpcg_mean_rec[m_idx] = mean(res_dpcg[3][m_idx,:])
		dpcg_var_rec[m_idx] = sqrt(var(res_dpcg[3][m_idx,:]))
	end

	chol_mean_runtime = chol[1]/runtime_normalization
	chol_mean_error = chol[2]

	ticks = 10. .^ collect(-3:1)

	pgfplotsx()
	
	plot(rb_mtsf_runtime,rb_mtsf_mean_approx,marker=4,markerstrokecolor=:auto,label="MTSF",xlabel="Time in s",ylabel="Approximation error",yaxis=:log,xaxis=:log,legend=:bottomright,ticks=true,linewidth=2,ylims=(10^(-13),10^(-4.05)))
	plot!(rb_cv_mtsf_runtime,rb_cv_mtsf_mean_approx,marker=4,markerstrokecolor=:auto,label="MTSF+GS",linewidth=2)
	plot!(cg_runtime,cg_mean_approx,marker=4,markerstrokecolor=:auto,label="CG",linewidth=2)
	plot!(dpcg_runtime,dpcg_mean_approx,marker=4,markerstrokecolor=:auto,label="D^{-1}+CG",linewidth=2)
	xticks!(ticks)
	if !ill_cond
		plot!(pcg_runtime,pcg_mean_approx,marker=4,markerstrokecolor=:auto,label="ILU+CG",linewidth=2)
	end
	vline!([chol_mean_runtime],line=:dot,label="",seriescolor=palette(:default)[6],linewidth=2)
	savefig("$(save_location)/$(g_str)_sm_approx.tikz")

	plot(rb_mtsf_runtime,rb_mtsf_mean_rec,marker=4,markerstrokecolor=:auto,label="MTSF",xlabel="Time in s",ylabel="Reconstruction error",xaxis=:log,yaxis=:log,legend=:topright,ticks=true,linewidth=2)
	plot!(rb_cv_mtsf_runtime,rb_cv_mtsf_mean_rec,marker=4,markerstrokecolor=:auto,label="MTSF+GS",linewidth=2)
	plot!(cg_runtime,cg_mean_rec,marker=4,markerstrokecolor=:auto,label="CG",linewidth=2)
	plot!(dpcg_runtime,dpcg_mean_rec,marker=4,markerstrokecolor=:auto,label="D^{-1}+CG",linewidth=2)
	xticks!(ticks)
	if !ill_cond
		plot!(pcg_runtime,pcg_mean_rec,marker=4,markerstrokecolor=:auto,label="ILU+CG",linewidth=2)
	end
	vline!([chol_mean_runtime],line=:dot,label="",seriescolor=palette(:default)[6],linewidth=2)
	hline!([chol_mean_error],line=:dot,label="EXACT",seriescolor=palette(:default)[7],linewidth=2)
	hline!([noisy_mean_error],line=:dot,label="INIT",seriescolor=palette(:default)[8],linewidth=2)
	savefig("$(save_location)/$(g_str)_sm_rec.tikz")

	gr()

	plot(rb_mtsf_runtime,rb_mtsf_mean_approx,marker=4,markerstrokecolor=:auto,label="MTSF",xlabel="Time in s",ylabel="Approximation error",yaxis=:log,xaxis=:log,legend=:bottomright,ticks=true,linewidth=2,ylims=(10^(-13),10^(-4.05)))
	plot!(rb_cv_mtsf_runtime,rb_cv_mtsf_mean_approx,marker=4,markerstrokecolor=:auto,label="MTSF+GS",linewidth=2)
	plot!(cg_runtime,cg_mean_approx,marker=4,markerstrokecolor=:auto,label="CG",linewidth=2)
	plot!(dpcg_runtime,dpcg_mean_approx,marker=4,markerstrokecolor=:auto,label="D^{-1}+CG",linewidth=2)
	xticks!(ticks)
	if !ill_cond
		plot!(pcg_runtime,pcg_mean_approx,marker=4,markerstrokecolor=:auto,label="ILU+CG",linewidth=2)
	end
	vline!([chol_mean_runtime],line=:dot,label="",seriescolor=palette(:default)[6],linewidth=2)
	savefig("$(save_location)/$(g_str)_sm_approx.pdf")

	plot(rb_mtsf_runtime,rb_mtsf_mean_rec,marker=4,markerstrokecolor=:auto,label="MTSF",xlabel="Time in s",ylabel="Reconstruction error",xaxis=:log,yaxis=:log,legend=:topright,ticks=true,linewidth=2)
	plot!(rb_cv_mtsf_runtime,rb_cv_mtsf_mean_rec,marker=4,markerstrokecolor=:auto,label="MTSF+GS",linewidth=2)
	plot!(cg_runtime,cg_mean_rec,marker=4,markerstrokecolor=:auto,label="CG",linewidth=2)
	plot!(dpcg_runtime,dpcg_mean_rec,marker=4,markerstrokecolor=:auto,label="D^{-1}+CG",linewidth=2)
	xticks!(ticks)
	if !ill_cond
		plot!(pcg_runtime,pcg_mean_rec,marker=4,markerstrokecolor=:auto,label="ILU+CG",linewidth=2)
	end
	vline!([chol_mean_runtime],line=:dot,label="",seriescolor=palette(:default)[6],linewidth=2)
	hline!([chol_mean_error],line=:dot,label="EXACT",seriescolor=palette(:default)[7],linewidth=2)
	hline!([noisy_mean_error],line=:dot,label="INIT",seriescolor=palette(:default)[8],linewidth=2)
	savefig("$(save_location)/$(g_str)_sm_rec.pdf")

	write(joinpath(save_location,"info.txt"),dump_string)
	
	return
end

for i in 1:5
	_,_,g_str,_,_,_,_ = generate_graph(i)
	run_measurements(i,"../data/runtime/smoothing/")
	plot_runtimes("../data/runtime/smoothing/$(g_str)/output.jld","../plots/simods/smoothing/$(g_str)")
end

