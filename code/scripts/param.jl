using BenchmarkTools, JLD, Plots
include("../src/header.jl")

n_range = UInt64.([10000]) # fixed size since we found it's not relevant

deg_range = Float64[50, 100, 150, 200]

q_range = Float64.(10. .^ [-3,-2,-1,0])

n_er = UInt64(10)

BenchmarkTools.DEFAULT_PARAMETERS.samples = 10000
BenchmarkTools.DEFAULT_PARAMETERS.seconds = Inf
LinearAlgebra.BLAS.set_num_threads(1)

function runtime_measurements(n_range::Vector{UInt64},deg_range::Vector{Float64},q_range::Vector{Float64},n_er::UInt64;save_directory::String="")

	mtsf_runtime = zeros(Float64, length(n_range), length(deg_range), n_er, length(q_range))
	mult_runtime = zeros(Float64, length(n_range), length(deg_range), n_er, length(q_range))

	for n_idx in 1:length(n_range)
		n = n_range[n_idx]
		base_signal = randn(ComplexF64,n)
		for deg_idx in 1:length(deg_range)
			p = deg_range[deg_idx] / n
			@info "deg $(deg_range[deg_idx])"
			for er_idx in 1:n_er
				g = Graphs.erdos_renyi(n,p)
				while !is_connected(g)
					g = Graphs.erdos_renyi(n,p)
				end

				d = Graphs.degree(g)

				Y = ComplexF64.(adjacency_matrix(g))
				x = unit_part.(randn(ComplexF64,n))
				multiplicative_perturbation!(Y,x)
				weight_matrix = abs.(Y)
				connection_matrix = unit_part.(Y)

				CG = ConnectionGraph(g,sparse(connection_matrix),sparse(weight_matrix))
				
				for q_idx in 1:length(q_range)
					qp = q_range[q_idx]
					qp *= mean(degree(g))
					q_vector = qp * ones(n)

					@info "$(Int64.([n_idx,deg_idx,er_idx,q_idx])) / $(Int64.([length(n_range),length(deg_range),n_er,length(q_range)]))"
 					b = @benchmarkable smooth_with_MTSF($CG,$q_vector,$base_signal,$1)
					mtsf_benchmark = run(b)
					
					b = @benchmarkable ($(CG.L)*$base_signal)
					mult_benchmark = run(b)
					
					mtsf_runtime[n_idx,deg_idx,er_idx,q_idx] = mean(mtsf_benchmark.times)
					mult_runtime[n_idx,deg_idx,er_idx,q_idx] = mean(mult_benchmark.times)
				end
			end
		end
	end

	if save_directory != ""
		save("$(save_directory)/param.jld","res",[mtsf_runtime,mult_runtime])
	end

	return [mtsf_runtime, mult_runtime]
end

# refaire les plots aussi

function plot_runtimes(mtsf_runtime::Array{Float64,4},mult_runtime::Array{Float64,4},n_range::Vector{UInt64},deg_range::Vector{Float64},q_range::Vector{Float64},n_er::UInt64,save_location::String)
	normal = 1e9
	mean_mtsf_runtime = mean(mtsf_runtime,dims=3) ./ normal # average over the random graphs
	mean_mult_runtime = mean(mult_runtime,dims=3) ./ normal # average over the random graphs

	for n_idx in 1:length(n_range)
		n = n_range[n_idx] ; n_str = "$(n)"
		for deg_idx in 1:length(deg_range)
			deg = deg_range[deg_idx] ; deg_str = "$(deg)"
			for q_idx in 1:length(q_range)
				q = q_range[q_idx] ; q_str = "$(q)"

				# variation in q
				plot(q_range,mean_mtsf_runtime[n_idx,deg_idx,1,:],xaxis=:log,yaxis=:log,xlabel="q'",ylabel="Time in s",label="MTSF",marker=4,seriescolor=palette(:default)[1],markerstrokecolor=:auto,linewidth=2)
				plot!(q_range,mean_mult_runtime[n_idx,deg_idx,1,:],xaxis=:log,yaxis=:log,label="L*x",marker=4,seriescolor=palette(:default)[3],markerstrokecolor=:auto,linewidth=2)
				xticks!(q_range)
				savefig("$(save_location)/q_variation_n_$(n_str)_deg_$(deg_str).pdf")

				# variation in deg
				plot(deg_range,mean_mtsf_runtime[n_idx,:,1,q_idx],yaxis=:log,xlabel="Mean degree",ylabel="Time in s",label="MTSF",marker=4,seriescolor=palette(:default)[1],markerstrokecolor=:auto,linewidth=2)
				plot!(deg_range,mean_mult_runtime[n_idx,:,1,q_idx],yaxis=:log,label="L*x",marker=4,seriescolor=palette(:default)[3],markerstrokecolor=:auto,linewidth=2)
				xticks!(deg_range)
				savefig("$(save_location)/deg_variation_n_$(n_str)_q_$(q_str).pdf")
			end
		end
	end
	return
end

function new_plots(file_location::String,save_location::String)
	normal = 1e9
	mtsf_runtime, mult_runtime = load(file_location)["res"]

	mtsf_mean_runtime = mean(mtsf_runtime,dims=3) ./ normal
	mult_mean_runtime = mean(mult_runtime,dims=3) ./ normal

	pgfplotsx()

	plot(deg_range,mtsf_mean_runtime[1,:,1,1],yaxis=:log,xlabel="Mean degree",ylabel="Time in s",label="MTSF, q' = 10e-3",marker=:circle,seriescolor=palette(:default)[1],markerstrokecolor=palette(:default)[1],linewidth=2,legend=:bottomright,ls=:dot)
#	plot!(deg_range,mtsf_mean_runtime[1,:,1,2],yaxis=:log,xlabel="Mean degree",ylabel="Time in s",label="MTSF",marker=:circle,seriescolor=palette(:default)[1],markerstrokecolor=:match,linewidth=2,markerstrokewidth=0)
	plot!(deg_range,mtsf_mean_runtime[1,:,1,3],yaxis=:log,xlabel="Mean degree",ylabel="Time in s",label="MTSF, q' = 10e-1",marker=:circle,seriescolor=palette(:default)[1],markerstrokecolor=palette(:default)[1],linewidth=2,ls=:dash)
	plot!(deg_range,mtsf_mean_runtime[1,:,1,4],yaxis=:log,xlabel="Mean degree",ylabel="Time in s",label="MTSF, q' = 1",marker=:circle,seriescolor=palette(:default)[1],markerstrokecolor=palette(:default)[1],linewidth=2)
	
	plot!(deg_range,mult_mean_runtime[1,:,1,1],yaxis=:log,xlabel="Mean degree",ylabel="Time in s",label="L*x, q' = 10e-3",marker=:diamond,seriescolor=palette(:default)[3],markerstrokecolor=palette(:default)[3],linewidth=2,ls=:dot,markerstrokewidth=0)
#	plot!(deg_range,mult_mean_runtime[1,:,1,2],yaxis=:log,xlabel="Mean degree",ylabel="Time in s",label="L*x",marker=:diamond,seriescolor=palette(:default)[3],markerstrokecolor=:match,linewidth=2,markerstrokewidth=0)
	plot!(deg_range,mult_mean_runtime[1,:,1,3],yaxis=:log,xlabel="Mean degree",ylabel="Time in s",label="L*x, q' = 10e-1",marker=:diamond,seriescolor=palette(:default)[3],markerstrokecolor=palette(:default)[3],linewidth=2,ls=:dash)
	plot!(deg_range,mult_mean_runtime[1,:,1,4],yaxis=:log,xlabel="Mean degree",ylabel="Time in s",label="L*x, q' = 1",marker=:diamond,seriescolor=palette(:default)[3],markerstrokecolor=palette(:default)[3],linewidth=2)

	xticks!(deg_range)

	#savefig("$(save_location)/param_variation.pdf")
	savefig("$(save_location)/param_variation.tikz")

	return;
end
