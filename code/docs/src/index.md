# MTSFs for Synchronization

A partial documentation for the code used in our papier "Random Multi-Type Spanning Forests for Synchronization on Sparse Graphs", including our implementations of the sampling and smoothing algorithms.

In the following, we always use the following representation for nodes.

```@docs
Node
```

```@contents
```

# Data Structures

```@docs
ConnectionGraph
```
```@docs
WeightedConnectionGraph
```
```@docs
NeighborInfo
```
```@docs
AliasBuffer
```
```@docs
SmoothingMTSF
```
```@docs
Counter
```
```@docs
SamplingBuffer
```

## Constructors

```@docs
ConnectionGraph(graph::SimpleGraph{UInt64},connection_matrix::SparseMatrixCSC{ComplexF64, Int64},weight_matrix::SparseMatrixCSC{Float64, Int64})
```
```@docs
NeighborInfo(v::Node,deg::Vector{UInt64})
```
```@docs
AliasBuffer(CG::WeightedConnectionGraph)
```
```@docs
SmoothingMTSF(n::UInt64)
```
```@docs
SamplingBuffer(n::UInt64)
```

## In-place initializers

```@docs
init_MTSF!(phi::SmoothingMTSF,n::UInt64)
```
```@docs
init_buffer!(buf::SamplingBuffer,n::UInt64)
```

# MTSF Sampling

```@docs
sample_MTSF!
```

# Smoothing operators on MTSFs

```@docs
smooth_with_MTSF
```
```@docs
rb_smooth_with_MTSF
```
```@docs
rb_cv_smooth_with_MTSF
```
```@docs
_propagate_in_MTSF!
```
```@docs
_propagate_average_in_MTSF!
```

# Miscellaneous utilitary functions
```@docs
aligned_l2_loss
```
```@docs
unitary_aligned_l2_loss
```
```@docs
epsilon_graph
```
```@docs
random_spanning_tree
```
```@docs
min_spanning_tree
```
```@docs
ust_synchronization
```
```@docs
mst_synchronization
```
```@docs
multiplicative_perturbation!
```
